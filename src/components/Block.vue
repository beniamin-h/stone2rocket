<template>
  <div :class="[
        'block',
        ...(block.kind),
        {
          'selected': selected,
          'highlighted': highlighted,
          'with-building': block.building,
        }]"
       v-tooltip="blockTooltip(block)"
       @click="blockSelected(block, $event)">
    {{ block.building &&
      buildingPhasesWithoutGraphics.includes(block.building.constructor.developmentPhase) &&
      block.building.constructor.displayName || '' }}
    <div :class="['building', block.building.codeName]"
         v-if="block.building">
      {{ metricPrefix(block.building.level) }}
      <span v-show="block.building.affordLevelUp"
            class="afford-level-up">▲</span>
    </div>
    <div class="disabled"
         v-show="block.building && block.building.disabledByInsufficientRes">✖</div>
  </div>
</template>

<script>
import {Game} from '@/core/objects/game';
import Block from '@/core/objects/block';
import {metricPrefix} from '@/utils/numUtils';
import { resObjToPrettyString } from '@/utils/tooltip';
import * as developmentPhases from '@/core/objects/buildings/developmentPhases';
import { prettyCase } from '@/utils/stringUtils';

export default {
  name: 'BlockMap',
  props: {
    blockRef: Block
  },
  data() {
    return {
      block: {},
      selected: false,
      highlighted: false,
      buildingPhasesWithoutGraphics: [
        developmentPhases.lateNeolithicArtistsPhase,
        developmentPhases.lateNeolithicCopperPhase,
        developmentPhases.earlyChalcolithicPhase,
        developmentPhases.middleChalcolithicPhase,
        developmentPhases.lateChalcolithicPhase,
      ],
      tooltipContainer: null,
      selectedToConstruct: null,
    };
  },
  created() {
    this.block = this.blockRef;
    Game
      .$on('UI/blockSelected', (block) => {
        if (this.block !== block) {
          this.selected = false;
        }
      })
      .$on('UI/buildingSelected', (building) => {
        this.selected = building !== null && this.block.building === building;
      })
      .$on('UI/highlightedBuilding', (building) => {
        this.highlighted = this.block.building instanceof building;
      })
      .$on('UI/unhighlightedBuilding', (building) => {
        if (this.block.building instanceof building) {
          this.highlighted = false;
        }
      })
      .$on('UI/selectedToConstruct', (building) => {
        this.selectedToConstruct = building;
      });
    Game.initialized.then(() => {
      this.tooltipContainer = document.querySelector('.block-map-inner');
    });
  },
  methods: {
    blockSelected(block, $event) {
      Game.$emit('UI/blockSelected', block, $event.shiftKey);
    },
    blockTooltip(block) {
      if (this.selectedToConstruct) {
        if (block.building) {
          return '';
        }
        return this._settlementDistanceTooltip(block, this.selectedToConstruct);
      }
      if (block.building) {
        return this._buildingInfoTooltip(block.building);
      }
    },
    _settlementDistanceTooltip(block, constructingBuilding) {
      let multiplier = '';
      if (constructingBuilding.hasAnyNetProduction()) {
        const multiplierValue = constructingBuilding.getSettlementDistanceNetMultiplier(
          Game.blockMap.getSettlementDistance(block.x, block.y)
        );
        multiplier = `x${multiplierValue.toFixed(2)}`;
      } else {
        multiplier = '-';
      }
      return this._blockTooltip(
        `Resource production multiplier from the distance to Settlement: ` +
          `<span class="green">${multiplier}</span>`,
        300);
    },
    _buildingInfoTooltip(building) {
      const gain = resObjToPrettyString(building.getTotalGain());
      const level = 'Level: ' + building.level;
      let net = resObjToPrettyString(building.getResNet(Game.exp, Game.blockMap));
      net += net ? ' /&nbsp;turn' : '';
      const deficit = building.disabledByInsufficientRes
        ? '<span class="red">' +
            'This building is disabled due to lack of the input resource: ' +
            prettyCase(building.insufficientRes) +
          '</span>'
        : '';
      return this._blockTooltip(
        [building.displayName, level, gain, net, deficit]
          .filter((v) => v)
          .join('<br />')
      );
    },
    _blockTooltip(content, showDelay = 500) {
      return {
        'content': content,
        'delay': {'show': showDelay, 'hide': 0},
        'placement': 'bottom',
        'boundariesElement': this.tooltipContainer,
      };
    },
    metricPrefix: metricPrefix,
  }
}
</script>

<style>
  .constructing .block:not(.with-building):hover {
    box-shadow: inset 0 0 16px -3px rgba(255, 255, 255, 0.75);
  }

  .block-map-inner:not(.constructing) .block:not(.selected) .building:hover {
    box-shadow: inset 0 0 150px rgba(255, 255, 255, 0.15);
  }
</style>

<style scoped>
  .block {
    width: 70px;
    height: 70px;
    box-sizing: border-box;
    float: left;
    position: relative;
    font-size: 12px;
    line-height: 1.3em;
    color: white;
    padding: 7px 12px 7px 8px;
  }

  .block.selected .building {
    font-weight: bold;
    box-shadow: inset 0 0 20px -3px rgba(0, 0, 0, 0.39);
  }

  .block.highlighted .building {
    box-shadow: inset 0 0 150px rgba(255, 255, 255, 0.25);
    background-color: #6da82499;
  }

  .building {
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    background-size: 36px;
    background-repeat: no-repeat;
    background-position: center 9px;
    text-align: right;
    padding: 53px 7px 0 7px;
    color: black;
    font-size: 11px;
  }

  .disabled {
    position: absolute;
    top: 2px;
    right: 3px;
    color: #db0000;
    font-size: 13px;
  }

  .afford-level-up {
    font-size: 12px;
    line-height: 0;
    margin-right: -4px;
    color: rgba(0, 0, 0, .8);
    margin-left: -2px;
  }

  .building.settlement {
    background-image: url('../assets/buildings/paleolith/settlement.svg');
    background-size: 48px;
  }

  .building.wood_collector {
    background-image: url('../assets/buildings/paleolith/wood_collector.svg');
    background-size: 50px;
    background-position-y: 12px
  }

  .building.stone_collector {
    background-image: url('../assets/buildings/paleolith/stone_collector.svg');
    background-size: 50px;
  }

  .building.food_collector {
    background-image: url('../assets/buildings/paleolith/food_collector.svg');
  }

  .building.encampment {
    background-image: url('../assets/buildings/paleolith/encampment.svg');
    background-position-y: 12px;
  }

  .building.sacred_place {
    background-image: url('../assets/buildings/paleolith/sacred_place.svg');
    background-size: 51px;
  }

  .building.altar {
    background-image: url('../assets/buildings/paleolith/altar.svg');
    background-size: 52px;
    background-position-y: 7px;
  }

  .building.wooden_tool_maker {
    background-image: url('../assets/buildings/paleolith/wooden_tool_maker.svg');
    background-size: 51px;
    background-position-y: 10px;
  }

  .building.reed_collector {
    background-image: url('../assets/buildings/paleolith/reed_collector.svg');
    background-size: 30px;
  }

  .building.reed_string_maker {
    background-image: url('../assets/buildings/paleolith/reed_string_maker.svg');
    background-size: 45px;
    background-position-x: 15px;
  }

  .building.wood_gatherer {
    background-image: url('../assets/buildings/paleolith/wood_gatherer.svg');
    background-size: 55px;
    background-position-y: 12px;
  }

  .building.food_gatherer {
    background-image: url('../assets/buildings/paleolith/food_gatherer.svg');
    background-size: 58px;
    background-position-y: 14px;
  }

  .building.huntsman {
    background-image: url('../assets/buildings/paleolith/huntsman.svg');
    background-size: 60px;
  }

  .building.basket_maker {
    background-image: url('../assets/buildings/paleolith/basket_maker.svg');
    background-size: 52px;
    background-position-y: 12px;
  }

  .building.straw_collector {
    background-image: url('../assets/buildings/paleolith/straw_collector.svg');
    background-size: 34px;
    background-position-y: 9px;
  }

  .building.fish_catcher {
    background-image: url('../assets/buildings/paleolith/fish_catcher.svg');
    background-size: 54px;
    background-position-y: 22px;
  }

  .building.cult_hut {
    background-image: url('../assets/buildings/paleolith/cult_hut.svg');
    background-size: 51px;
  }

  .building.stone_tool_maker {
    background-image: url('../assets/buildings/paleolith/stone_tool_maker.svg');
    background-size: 48px;
  }

  .building.scavenger_post {
    background-image: url('../assets/buildings/paleolith/scavenger_post.svg');
    background-size: 58px;
    background-position-y: 14px;
  }

  .building.earth_oven {
    background-image: url('../assets/buildings/paleolith/earth_oven.svg');
    background-size: 44px;
  }

  .building.clay_pit {
    background-image: url('../assets/buildings/paleolith/clay_pit.svg');
    background-size: 56px;
    background-position-y: 20px;
  }

  .building.wooden_spear_maker {
    background-image: url('../assets/buildings/paleolith/wooden_spear_maker.svg');
    background-size: 41px;
    background-position-y: 6px;
  }

  .building.wooden_spear_hunter {
    background-image: url('../assets/buildings/paleolith/wooden_spear_hunter.svg');
    background-size: 58px;
    background-position-y: 11px;
  }

  .building.stone_pick_pit {
    background-image: url('../assets/buildings/paleolith/stone_pick_pit.svg');
    background-size: 58px;
    background-position-y: 11px;
  }

  .building.stone_axe_post {
    background-image: url('../assets/buildings/paleolith/stone_axe_post.svg');
    background-size: 55px;
    background-position-y: 5px;
  }

  .building.wooden_tool_sharpener {
    background-image: url('../assets/buildings/paleolith/wooden_tool_sharpener.svg');
    background-size: 46px;
  }

  .building.pelt_string_maker {
    background-image: url('../assets/buildings/paleolith/pelt_string_maker.svg');
    background-size: 49px;
  }

  .building.bark_string_maker {
    background-image: url('../assets/buildings/paleolith/bark_string_maker.svg');
    background-size: 51px;
  }

  .building.obsidian_bead_maker {
    background-image: url('../assets/buildings/neolith/obsidian_bead_maker.svg');
    background-size: 25px;
    background-position-y: 10px;
  }

  .building.clay_figurine_maker {
    background-image: url('../assets/buildings/neolith/clay_figurine_maker.svg');
    background-size: 36px;
  }

  .building.bone_carver {
    background-image: url('../assets/buildings/neolith/bone_carver.svg');
    background-size: 48px;
  }

  .building.tar_pit {
    background-image: url('../assets/buildings/neolith/tar_pit.svg');
    background-size: 59px;
    background-position-y: 34px;
  }

  .building.tar_painter {
    background-image: url('../assets/buildings/neolith/tar_painter.svg');
    background-size: 52px;
    background-position-y: 14px;
  }

  .building.ochre_painter {
    background-image: url('../assets/buildings/neolith/ochre_painter.svg');
    background-size: 52px;
    background-position-y: 14px;
  }

  .building.charcoal_painter {
    background-image: url('../assets/buildings/neolith/charcoal_painter.svg');
    background-size: 58px;
    background-position-y: 13px;
  }

  .building.cave_painting {
    background-image: url('../assets/buildings/neolith/cave_painting.svg');
    background-size: 58px;
    background-position-y: 13px;
  }

  .building.pelt_sewer {
    background-image: url('../assets/buildings/neolith/pelt_sewer.svg');
    background-size: 50px;
    background-position-y: 7px;
  }

  .building.fire_pit {
    background-image: url('../assets/buildings/neolith/fire_pit.svg');
    background-size: 60px;
    background-position-y: 12px;
  }

  .building.tent_camp {
    background-image: url('../assets/buildings/neolith/tent_camp.svg');
    background-size: 60px;
    background-position-y: 10px;
  }

  .building.stone_tool_sharpener {
    background-image: url('../assets/buildings/neolith/stone_tool_sharpener.svg');
    background-size: 48px;
  }

  .building.stone_spear_maker {
    background-image: url('../assets/buildings/neolith/stone_spear_maker.svg');
    background-size: 41px;
    background-position-y: 6px;
  }

  .building.stone_spear_hunter {
    background-image: url('../assets/buildings/neolith/stone_spear_hunter.svg');
    background-size: 63px;
    background-position-y: 8px;
  }

  .building.charcoal_pile {
    background-image: url('../assets/buildings/neolith/charcoal_pile.svg');
    background-size: 40px;
    background-position-y: 7px;
  }

  .building.food_grinding_hut {
    background-image: url('../assets/buildings/neolith/food_grinding_hut.svg');
    background-size: 47px;
    background-position-y: 17px;
  }

  .building.smoke_hut {
    background-image: url('../assets/buildings/neolith/smoke_hut.svg');
    background-size: 55px;
    background-position-y: 19px;
  }

  .building.clay_excavation_site {
    background-image: url('../assets/buildings/neolith/clay_excavation_site.svg');
    background-size: 56px;
    background-position-y: 19px;
  }

  .building.vine_collector {
    background-image: url('../assets/buildings/neolith/vine_collector.svg');
    background-size: 54px;
    background-position-y: 4px;
  }

  .building.vine_string_maker {
    background-image: url('../assets/buildings/neolith/vine_string_maker.svg');
    background-size: 60px;
  }

</style>
