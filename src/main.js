// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import Vue from 'vue';
import App from './App';
import VTooltip from 'v-tooltip';
import VueDragscroll from 'vue-dragscroll'

if (process.env.NODE_ENV === 'production' && 1) { // TODO: disable!
  import('devtools-detector').then(({ addListener, launch }) => {
    addListener((isOpen) => {
      if (isOpen) {
        document.getElementById('app').remove();
      }
    });
    launch();
  });
}

Vue.config.productionTip = false;
Vue.use(VTooltip, {
  defaultPlacement: 'bottom',
  defaultHideOnTargetClick: false,
  defaultDelay: {
    show: 500, hide: 0,
  },
  // defaultTrigger: 'click',
});
Vue.use(VueDragscroll);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
});
