export default class Deferred extends Promise {
  constructor() {
    const deferred = {};
    super((resolve, reject) => {
      deferred.resolve = resolve;
      deferred.reject = reject;
    });
    this.resolve = deferred.resolve;
    this.reject = deferred.reject;
  }
}
