import prettyResNames from '@/core/prettyResNames';
import { metricPrefix } from '@/utils/numUtils';

/**
 * Flattens obj to one string joining values (numbers expected) and keys.
 * @param obj - the object to flat
 * @param showPlusSign - show '+' for positive values
 * @param sep - joining separator of items
 * @returns {string}
 */
export function resObjToPrettyString(obj, showPlusSign = false, sep = ', ') {
  return Object.entries(obj)
    .map(([resName, resAmt]) => {
      return (showPlusSign && resAmt > 0 ? '+' : '') +
        metricPrefix(resAmt) + '&nbsp;' +
        prettyResNames[resName];
    })
    .join(sep);
}

/**
 * Flattens resObj to one string joining values and keys
 * comparing its values with refObjRefer values.
 * @param resObj - the base object to flat
 * @param resObjToCompare - referential object to compare values with
 * @param showPlusSign - show '+' for positive values
 * @param sep - joining separator of items
 * @param compareOnlyNegatives - suitable to compare 'net' instead of 'store'
 * @returns {string}
 */
export function resObjToPrettyStringWithRed(
  resObj, resObjToCompare, {
    showPlusSign = false,
    sep = ', ',
    compareOnlyNegatives = false
  }
) {
  return Object.entries(resObj)
    .map(([resName, resAmt]) => {
      const value = (showPlusSign && resAmt > 0 ? '+' : '') +
        metricPrefix(resAmt) + '&nbsp;' +
        prettyResNames[resName];
      let markRed;
      if (compareOnlyNegatives) {
        markRed = resAmt < 0 && resObjToCompare[resName] < -resAmt;
      } else {
        markRed = resObjToCompare[resName] < resAmt;
      }
      if (markRed) {
        return `<span class="red">${value}</span>`;
      }
      return value;
    })
    .join(sep);
}

/**
 * Returns proportions of values between the given obj and stored amounts.
 * @param obj - the resource object
 * @param resStore - resStore instance
 * @returns {{}}
 */
export function getFractionsOfStored(obj, resStore) {
  return Object.entries(obj)
    .reduce((result, [resName, resAmt]) => {
      result[resName] = resAmt / resStore.store[resName];
      return result;
    }, {});
}

/**
 * Flattens and object of proportions (fractions) to a single string.
 * @param fractions - the proportion object
 * @param prefix - value to set before each item
 * @param suffix - value to set after each item
 * @param sep - item separator
 * @param markRed - mark values greater then 1 with red
 * @returns {string}
 */
export function resFractionsToPrettyString(
  fractions, {
    prefix = '(',
    suffix = ')',
    sep = '<br />',
    markRed = true,
  } = {},
) {
  return Object.entries(fractions)
    .map(([resName, fraction]) => {
      if (fraction === Infinity) {
        if (markRed) {
          return '<span class="red">-</span>';
        }
        return '-';
      }
      let value = '';
      if (fraction < 0.0099) {
        value = `<1%`;
      } else if (fraction > 1) {
        value = 'x' + metricPrefix(fraction);
      } else {
        value = Math.ceil(fraction * 100) + '%';
      }
      value = `${prefix}${value}${suffix}`;
      if (fraction > 1 && markRed) {
        return `<span class="red">${value}</span>`;
      }
      return value;
    })
    .join(sep);
}

/**
 * Returns a basic building tooltip HTML structure.
 * @param cost
 * @param costFraction
 * @param gain
 * @param net
 * @param costLabel
 * @param costFractionLabel
 * @param gainLabel
 * @param resNetLabel
 * @returns {string|string}
 */
export function buildingTooltip(cost, costFraction, gain, net, {
  costLabel = 'Build cost:',
  costFractionLabel = '% of stored',
  gainLabel = 'Build gain:',
  resNetLabel = 'Resource flow per turn:',
} = {}) {
  let costBlock = '';
  if (cost) {
    costBlock = `<div class="block">` +
      `<div class="block-label">${costLabel}</div> ${cost}` +
    `</div>` +
    `<div class="block fraction">` +
      `<div class="block-label">${costFractionLabel}</div> ${costFraction}` +
    `</div>`;
  }
  const content = [
    costBlock,
    gain ? `<div class="block"><div class="block-label">${gainLabel}</div> ${gain}</div>` : '',
    net ? `<div class="block"><div class="block-label">${resNetLabel}</div> ${net}</div>` : '',
  ].filter((v) => v).join('<div class="sep"></div>');
  return content ? `<div class="block-box">${content}</div>` : '';
}
