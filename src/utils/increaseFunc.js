export const noIncrease = (a, b) => a;
export const constIncrease = (a, b) => a * (b + 1);
export const incIncrease = (a, b) => b * (b + (a - 1) * b + 1) / 2 + 1;
export const expIncrease = (a, b) => Math.pow(a, b);
export const expRevIncrease = (a, b) => Math.pow(b + 1, a);
export const expExpIncrease = (a, b) => Math.pow(b + 1, a * b);
