import aes from 'crypto-js/aes';
import utf8 from 'crypto-js/enc-utf8';

const SECRET = 'addKey';

export function encrypt(text) {
  return aes.encrypt(text, SECRET).toString();
}

export function decrypt(text) {
  return aes.decrypt(text, SECRET).toString(utf8);
}
