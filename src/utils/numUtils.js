import Big from 'big.js';

const ZeroIDX = 11;
const prefixes = 'vwxyzafpnumkMBT'.split('');
prefixes.push(
  'Qa', 'Qi', 'Sx', 'Sp', 'Oc', 'No',
  'Dc', 'UDc', 'DDc', 'TDc', 'QaDc', 'QiDc', 'SxDc', 'SpDx', 'OcDx', 'NoDx',
  'Vi', 'UVi', 'DVi', 'TVi', 'QaVi', 'QiVi', 'SxVi', 'SpVi', 'OcVi', 'NoVi'
);
prefixes.splice(ZeroIDX, 0, ''); // insert neutral prefix

export function metricPrefix(number, {
  unit = '',
  precision = 3,
  delimiter = '\xa0'
} = {}) {
  let N;
  // try {
  N = (number instanceof Big) ? number : new Big(number);
  // } catch (e) {
  //   console.error(e);
  //   debugger;
  // }
  let prefix = prefixes[ZeroIDX + Math.floor(N.e / 3)];
  if (prefix === undefined) {
    return number.toExponential(2);
  }
  precision += N.e >= 0 ? N.e % 3 : 0;
  N.c = N.c.slice(0, precision);
  N.e = (N.e >= 0) ? N.e % 3 : 2 + ((N.e + 1) % 3);
  const dl = (prefix === '' && unit === '') ? '' : delimiter;
  const n = N.toPrecision(precision).split('');
  if (n.indexOf('.') !== 0) {
    while (n[n.length - 1] === '0') { n.pop(); }
    if (n[n.length - 1] === '.') { n.pop(); }
  }
  return `${n.join('')}${dl}${prefix}${unit}`;
}

export function negateValues(object) {
  return Object.entries(object).reduce((result, [key, value]) => {
    return { ...result, [key]: value * -1 };
  }, {});
}
