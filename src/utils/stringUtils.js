export function prettyCase(str) {
  str = str.replace(/([A-Z])/g, ' $1').trim().toLocaleLowerCase();
  return str.charAt(0).toUpperCase() + str.slice(1);
}

export function dashCase(str) {
  str = str.replace(/([A-Z])/g, '_$1').trim().toLocaleLowerCase();
  return str.slice(1);
}
