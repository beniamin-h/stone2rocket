export const kinds = [
  'land',
  'forest',
  'mountain',
];

export default kinds.reduce((map, elem) => {
  map[elem] = elem;
  return map;
}, {});
