import {
  settlementPhase,
  believersPhase,
  stonePhase,
  neolithicFirePhase,
  lateNeolithicCopperPhase,
  earlyChalcolithicPhase,
  middleChalcolithicPhase,
  lateChalcolithicPhase,
} from '@/core/objects/buildings/developmentPhases';

export const resGrouped = [
  [
    'workforce',
    'wood',
    'stone',
    'food',
    'holy',
    'belief',
  ], [
    'woodenTools',
    'reed',
    'string',
    'animalPelt',
    'animalBone',
    'basket',
    'straw',
    'rawMeat',
  ], [
    'stoneTools',
    'charcoal',
    'clay',
    'ochre',
    'woodenSpear',
    'bark',
    'obsidian',
    'mineralPigments',
    'tar',
    'neolithicArt',
  ], [
    'neolithicCulture',
    'peltClothing',
    'preciousStone',
    'stoneSpear',
    'bow',
    'arrow',
    'grain',
    'flour',
    'rawHide',
    'vine',
  ], [
    'copperOre',
    'copper',
    'copperTools',
    'fruit',
    'fruitLiquor',
  ], [
    'chalcolithicCulture',
    'pottery',
    'waterSupply',
    'sand',
    'brick',
    'reedBoat',
  ], [
    'milk',
    'grapes',
    'wine',
    'elementalDeity',
    'elementalGodWorship',
    'animalDeity',
    'animalGodWorship',
    'humanDeity',
    'humanGodWorship',
    'bread',
  ], [
    'tablet',
    'zigguratConstruction',
  ]
];

export const resList = resGrouped.flat();

export const resGroupToDevPhase = [
  settlementPhase,
  believersPhase,
  stonePhase,
  neolithicFirePhase,
  lateNeolithicCopperPhase,
  earlyChalcolithicPhase,
  middleChalcolithicPhase,
  lateChalcolithicPhase,
];

export const resValues = resList.reduce((result, elem, index) => {
  result[elem] = Math.pow(1.2, index);
  return result;
}, {});

const resObj = resList.reduce((map, elem) => {
  map[elem] = elem;
  return map;
}, {});

export default resObj;
