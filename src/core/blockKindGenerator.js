import { kinds } from '@/core/consts/blockKind';

export function generateBlockKind() {
  return kinds[Math.floor(Math.random() * 3)];
}
