export default class Stats {
  turnsByPhase = {};

  initWatchers(buildingProviderEventBus, game) {
    buildingProviderEventBus
      .$on('lastPhaseBuiltChanged', (phase) => {
        if (!(phase in this.turnsByPhase)) {
          this.turnsByPhase[phase] = game.turn;
        }
      });
  }

  serialize() {
    return {
      turnsByPhase: this.turnsByPhase,
    }
  }

  load(statsSave) {
    for (const phase in statsSave.turnsByPhase) {
      this.turnsByPhase[phase] = statsSave.turnsByPhase[phase];
    }
  }
};
