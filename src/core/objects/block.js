import md5 from 'md5';

export default class Block {
  constructor (x, y, kind, id) {
    this.kind = kind;
    this.x = x;
    this.y = y;
    this.id = id || md5('' + Math.random());
    this.building = null;
  }

  serialize() {
    return {
      id: this.id,
      kind: this.kind,
      x: this.x,
      y: this.y,
    };
  }
}
