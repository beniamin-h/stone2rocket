import Vue from 'vue';
import buildingClasses, { buildingValues } from '@/core/objects/buildings';
import { resList, resValues } from '@/core/consts/res';

export default class Exp {
  ADD_KIND = {
    BUILDING: 'building',
    RES_NET: 'res_net',
    RES_STORE: 'res_store',
    GAME: 'game',
  };

  GET_KIND = {
    BUILDING: 'building',
    RES: 'res',
    GAME: 'game',
  };

  GAME_EXP_VALUE_FACTOR = {
    [this.ADD_KIND.BUILDING]: 60_000,
    [this.ADD_KIND.RES_NET]: 30_000,
    [this.ADD_KIND.RES_STORE]: 1,
  };

  RES_STORE_VALUE_DENOMINATOR = 10_000;
  BASE_EXP_PER_LEVEL = {
    [this.GET_KIND.BUILDING]: 100,
    [this.GET_KIND.RES]: 10_000,
    [this.GET_KIND.GAME]: 100_000,
  };

  ADD_KIND_TO_GET_KIND = {
    [this.ADD_KIND.BUILDING]: this.GET_KIND.BUILDING,
    [this.ADD_KIND.RES_STORE]: this.GET_KIND.RES,
    [this.ADD_KIND.RES_NET]: this.GET_KIND.RES,
  };

  resStore = null;
  buildingProvider = null;
  expStore = null;
  eventBus = null;

  constructor (resStore, buildingProvider) {
    this.resStore = resStore;
    this.buildingProvider = buildingProvider;
    this.initExpStore(buildingClasses, resList);
    this.eventBus = new Vue();
  }

  initExpStore(buildingClasses, resList) {
    this.expStore = {
      [this.GET_KIND.BUILDING]:
        buildingClasses.reduce((result, buildingClass) => {
          return { ...result, [buildingClass.className]: 0 };
        }, {}),
      [this.GET_KIND.RES]:
        resList.reduce((result, resName) => {
          return { ...result, [resName]: 0 };
        }, {}),
      [this.GET_KIND.GAME]: 0,
    }
  }

  getGameExpValueAdd(expKind, key, value) {
    let baseFactor = this.GAME_EXP_VALUE_FACTOR[expKind] * value;
    if (expKind === this.ADD_KIND.RES_NET || expKind === this.ADD_KIND.RES_STORE) {
      baseFactor *= resValues[key];
    }
    if (expKind === this.ADD_KIND.BUILDING) {
      baseFactor *= buildingValues[key];
    }
    return Math.ceil(Math.pow(baseFactor / 30_000, 2));
  }

  addExp(expAddKind, key, value) {
    if (value === 0) {
      return;
    }
    let gameKindValue = value;
    let expValue = value;
    if (expAddKind !== this.ADD_KIND.GAME) {
      if (expAddKind === this.ADD_KIND.RES_STORE) {
        expValue /= this.RES_STORE_VALUE_DENOMINATOR;
      }
      this.expStore[this.ADD_KIND_TO_GET_KIND[expAddKind]][key] += Math.ceil(expValue);
      gameKindValue = this.getGameExpValueAdd(expAddKind, key, value);
    }
    this.expStore[this.GET_KIND.GAME] += gameKindValue;
  }

  tickExp() {
    this.buildingProvider.buildings.forEach((building) => {
      this.addExp(this.ADD_KIND.BUILDING, building.constructor.className, building.level);
    });
    Object.entries(this.resStore.produced).forEach(([resName, resAmt]) => {
      this.addExp(this.ADD_KIND.RES_NET, resName, resAmt);
    });
    Object.entries(this.resStore.store).forEach(([resName, resAmt]) => {
      this.addExp(this.ADD_KIND.RES_STORE, resName, resAmt);
    });
    this.addExp(this.ADD_KIND.GAME, null, 10);
    this.eventBus.$emit('expTicked');
  }

  getExp(expKind, key = null) {
    if (expKind === this.GET_KIND.GAME) {
      return this.expStore[expKind];
    }
    return this.expStore[expKind][key];
  }

  getLevel(expKind, key = null) {
    const exp = this.getExp(expKind, key);
    return Math.ceil(
      Math.log((exp / this.BASE_EXP_PER_LEVEL[expKind] + 11) / 10) /
      Math.log(1.1) - 1
    );
  }

  getLevelExp(level, expKind, key = null) {
    return ((Math.pow(1.1, level + 1) - 1) * 10 - 1) *
      this.BASE_EXP_PER_LEVEL[expKind];
  }

  getNextLevelProgress(expKind, key = null) {
    const level = this.getLevel(expKind, key);
    const currentLevelExpMin = this.getLevelExp(level - 1, expKind, key);
    const nextLevelExpMin = this.getLevelExp(level, expKind, key);
    const currentLevelExpEarned = this.getExp(expKind, key) - currentLevelExpMin;
    const nextLevelDiff = nextLevelExpMin - currentLevelExpMin;
    return currentLevelExpEarned / nextLevelDiff;
  }

  serialize() {
    return {
      expStore: this.expStore,
    };
  }

  load(expSave) {
    for (const building in expSave.expStore[this.GET_KIND.BUILDING]) {
      this.expStore[this.GET_KIND.BUILDING][building] = expSave.expStore[this.GET_KIND.BUILDING][building];
    }
    for (const resName in expSave.expStore[this.GET_KIND.RES]) {
      this.expStore[this.GET_KIND.RES][resName] = expSave.expStore[this.GET_KIND.RES][resName];
    }
    this.expStore[this.GET_KIND.GAME] = expSave.expStore[this.GET_KIND.GAME];
  }
}
