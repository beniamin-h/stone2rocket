import Vue from 'vue';
import { getNextPhase, isNextPhase, isPhaseLater } from '@/core/objects/buildings/developmentPhases';
import { buildingsByPhase, buildings } from '@/core/objects/buildings';

export default class BuildingProvider {
  constructor() {
    this.buildings = [];
    this.builtCounters = {};
    this.lastPhaseBuilt = null;
    this.undiscoveredBuildingCount = 0;
    this.unlockedBuildingCount = 0;
    this.isLastPhaseUnlocked = true;
    this.eventBus = new Vue();
  }

  construct(
    block, BuildingClass, resStore, exp, blockMap,
    baseLevel = 1, forFree = false,
  ) {
    const building = new BuildingClass(block.x, block.y, baseLevel);
    if (!building.build(this, resStore, blockMap, forFree)) {
      return false;
    }
    if (!forFree) {
      building.hasSufficientInputRes(building.getResNet(exp, blockMap), resStore);
    }
    block.building = building;
    this.buildings.push(building);
    this.updateBuiltCounters(BuildingClass.className, 1);
    resStore.recalcResNet();
    this.updatePhases(BuildingClass);
    return building;
  }

  updateBuiltCounters(buildingClassName, change) {
    const current = this.builtCounters[buildingClassName] || 0;
    this.builtCounters[buildingClassName] = current + change;
  }

  updatePhases(BuildingClass) {
    if (isPhaseLater(BuildingClass.developmentPhase, this.lastPhaseBuilt)) {
      this.lastPhaseBuilt = BuildingClass.developmentPhase;
      const prevUndiscoveredBuildingCount = this.undiscoveredBuildingCount;
      this.undiscoveredBuildingCount = Object.keys(buildingsByPhase)
        .reduce((count, phase) => {
          if (isPhaseLater(phase, this.lastPhaseBuilt) && !isNextPhase(phase, this.lastPhaseBuilt)) {
            return count + buildingsByPhase[phase].length;
          }
          return count;
        }, 0);
      if (prevUndiscoveredBuildingCount) {
        const nextPhase = getNextPhase(this.lastPhaseBuilt);
        if (!nextPhase) {
          return;
        }
        this.unlockedBuildingCount = buildingsByPhase[nextPhase].length;
        this.isLastPhaseUnlocked = false;
      }
      this.eventBus.$emit(
        'lastPhaseBuiltChanged', this.lastPhaseBuilt,
      );
    }
  }

  remove(block, building, resStore, force = false) {
    const index = this.buildings.indexOf(building);
    if (index === -1) {
      return false;
    } else {
      if (!building.demolish(this, resStore, force)) {
        return false;
      }
      block.building = null;
      this.buildings.splice(index, 1);
      this.updateBuiltCounters(building.constructor.className, -1);
      resStore.recalcResNet();
      return true;
    }
  }

  changeLevel(
    building, resStore,
    maxLevel = false, fiveLevels = false, downgrade = false
  ) {
    let counter = 1;
    const changeLevelFunc = downgrade ? building.downgrade : building.upgrade;
    while (changeLevelFunc.call(building, resStore) &&
      // eslint-disable-next-line
      ((maxLevel && counter < building.constructor.maxLevelDiff) ||
        // eslint-disable-next-line
        (fiveLevels && counter < 5))
    ) { counter++; }
    resStore.recalcResNet();
    this.eventBus.$emit('buildingLvlChanged', building);
  }

  demolishAll(blockMap, resStore, exp) {
    [...this.buildings].forEach((building) => {
      const buildingBlock = blockMap.getBlockByXY(building.mapX, building.mapY);
      this.remove(buildingBlock, building, resStore, true);
    });
  }

  getSameClass(givenBuilding) {
    return this.buildings.filter((building) => {
      return building instanceof givenBuilding.constructor;
    });
  }

  getNearestSameClass(givenBuilding, offset) {
    const sortedBuildings = this.getSameClass(givenBuilding)
      .sort((buildingA, buildingB) => {
        if (buildingA.mapY === buildingB.mapY) {
          return buildingA.mapX - buildingB.mapX;
        }
        return buildingA.mapY - buildingB.mapY;
      });
    const givenIndex = sortedBuildings.indexOf(givenBuilding)
    return sortedBuildings[givenIndex + offset];
  }

  initAffordLevelUpUpdater(resStore) {
    resStore.eventBus.$on('netUpdated', () => {
      this._updateAffordLevelUp(resStore);
    });
  }

  _updateAffordLevelUp(resStore) {
    this.buildings.forEach((building) => {
      building.affordLevelUp = building.canBeUpgraded(resStore, 1);
    });
  }

  serialize() {
    return {
      buildings: this.buildings.reduce((buildings, building) => {
        buildings.push(building.serialize());
        return buildings;
      }, []),
      isLastPhaseUnlocked: this.isLastPhaseUnlocked,
    }
  }

  load(buildingProviderSave, blockMap, resStore, exp) {
    this.demolishAll(blockMap, resStore, exp);
    this.lastPhaseBuilt = null;
    buildingProviderSave.buildings.forEach((buildingSave) => {
      const building = this.construct(
        blockMap.getBlockByXY(buildingSave.mapX, buildingSave.mapY),
        buildings[buildingSave.className],
        resStore,
        exp,
        blockMap,
        buildingSave.level,
        true,
      );
      if (buildingSave.disabledByInsufficientRes) {
        building.disabledByInsufficientRes = true;
        building.insufficientRes = buildingSave.insufficientRes;
      }
    });
    this.isLastPhaseUnlocked = buildingProviderSave.isLastPhaseUnlocked ||
      buildingProviderSave.buildings.length === 1;
    if (buildingProviderSave.isLastPhaseUnlocked) {
      this.unlockedBuildingCount = 0;
    }
  }
}
