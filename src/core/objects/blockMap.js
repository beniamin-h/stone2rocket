import Block from './block';
import {generateBlockKind} from '@/core/blockKindGenerator';

export default class BlockMap {
  constructor() {
    this.blocks = {};
    this.mapSize = 0;
    this.settlementCoords = null;
  }

  generateMap(mapSize = 10, blocks = null) {
    this.mapSize = mapSize;
    for (let x = 0; x < mapSize; x++) {
      this.blocks[x] = {};
      for (let y = 0; y < mapSize; y++) {
        if (blocks) {
          const block = blocks[x][y];
          this.blocks[x][y] = new Block(block.x, block.y, block.kind, block.id);
        } else {
          this.blocks[x][y] = new Block(x, y, generateBlockKind(x, y));
        }
      }
    }
  }

  getBlockByXY(x, y) {
    return this.blocks[x][y];
  }

  getNearestBuildingBlockInRow(x, y) {
    for (let i = 0; i < this.mapSize; i++) {
      if (this.blocks[x + i] && this.blocks[x + i][y] && this.blocks[x + i][y].building) {
        return this.blocks[x + i][y];
      }
      if (this.blocks[x - i] && this.blocks[x - i][y] && this.blocks[x - i][y].building) {
        return this.blocks[x - i][y];
      }
    }
  }

  getNearestBuildingBlockInCol(x, y) {
    for (let i = 0; i < this.mapSize; i++) {
      if (this.blocks[x] && this.blocks[x][y + i] && this.blocks[x][y + i].building) {
        return this.blocks[x][y + i];
      }
      if (this.blocks[x] && this.blocks[x][y - i] && this.blocks[x][y - i].building) {
        return this.blocks[x][y - i];
      }
    }
  }

  setSettlementCoords(x, y) {
    this.settlementCoords = [x, y];
  }

  getSettlementDistance(x, y) {
    return Math.sqrt(
      Math.pow(this.settlementCoords[0] - x, 2) +
      Math.pow(this.settlementCoords[1] - y, 2)
    );
  }

  getSettlement() {
    return this.getBlockByXY(
      this.settlementCoords[0], this.settlementCoords[1]
    ).building;
  }

  serialize() {
    return {
      mapSize: this.mapSize,
      blocks: Object.keys(this.blocks).reduce((result, x) => {
        result[x] = Object.entries(this.blocks[x]).reduce((resultCol, [y, block]) => {
          resultCol[y] = block.serialize();
          return resultCol;
        }, {});
        return result;
      }, {}),
    }
  }

  load(blockMapSave) {
    this.generateMap(blockMapSave.mapSize, blockMapSave.blocks);
  }
}
