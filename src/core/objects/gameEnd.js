import Vue from 'vue';

export default class GameEnd {
  static victoriesEnabled = {
    demo: true,
  };

  eventBus;

  constructor() {
    this.eventBus = new Vue();
  }

  initVictories(buildingProviderEventBus) {
    if (this.constructor.victoriesEnabled.demo) {
      buildingProviderEventBus
        .$on('buildingLvlChanged', (building) => {
          if (building.constructor.className === 'TheGreatZiggurat' &&
              building.level === 10) {
            this.eventBus.$emit('victory', 'demo');
          }
        });
    }
  }
};
