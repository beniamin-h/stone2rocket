import res from '@/core/consts/res'
import Vue from 'vue';

export default class ResStore {
  store;
  net;
  netPerBuildingClass;
  produced;
  deficit;

  buildingProvider;
  exp;
  blockMap;
  eventBus;

  constructor () {
    this.initStore();
    this.initNet();
    this.initProduced();
    this.initDeficit();
    this.eventBus = new Vue();
  }

  setDependencies(buildingProvider, exp, blockMap) {
    this.buildingProvider = buildingProvider;
    this.exp = exp;
    this.blockMap = blockMap;
  }

  _getResNameIndexedObj(defaultValue = 0) {
    return Object.keys(res).reduce((result, resName) => {
      result[resName] = defaultValue;
      return result;
    }, {});
  }

  initStore() {
    const resSave = {'workforce': 0, 'wood': 83229411, 'stone': 32533671, 'food': 678586778, 'holy': 0, 'belief': 0, 'woodenTools': 857976, 'reed': 73343, 'string': 114986, 'animalPelt': 5977, 'animalBone': 5627, 'basket': 2028, 'straw': 69666, 'rawMeat': 0, 'stoneTools': 21577, 'charcoal': 0, 'clay': 0, 'ochre': 0, 'woodenSpear': 0, 'obsidian': 0, 'mineralPigments': 0, 'neolithicArt': 0, 'bark': 0, 'tar': 0, 'peltClothing': 0, 'preciousStone': 0, 'neolithicCulture': 0, 'stoneSpear': 0, 'bow': 0, 'arrow': 0, 'grain': 0, 'flour': 0, 'rawHide': 0, 'vine': 0, 'copperOre': 0, 'copper': 0, 'copperTools': 0, 'chalcolithicCulture': 0, 'fruit': 0, 'fruitLiquor': 0, 'pottery': 0, 'waterSupply': 0, 'milk': 0, 'animalDeity': 0};
    // const resSave = {'workforce': 0, 'wood': 50622792195, 'stone': 22799349200, 'food': 149977217893, 'holy': 0, 'belief': 0, 'woodenTools': 21400532, 'reed': 440638, 'string': 28662778, 'animalPelt': 4251713, 'animalBone': 41095553, 'basket': 96313, 'straw': 9430693, 'rawMeat': 18065290, 'stoneTools': 15107273, 'charcoal': 291213, 'clay': 3318528, 'ochre': 456637, 'woodenSpear': 30, 'obsidian': 153066, 'mineralPigments': 182406, 'neolithicArt': 0, 'bark': 3026772, 'tar': 41633, 'peltClothing': 193, 'preciousStone': 0, 'neolithicCulture': 0, 'rawHide': 11273, 'stoneSpear': 4819, 'bow': 0, 'arrow': 0, 'grain': 0, 'flour': 0, 'copperOre': 0, 'copper': 0, 'copperTools': 0, 'chalcolithicCulture': 0, 'vine': 1480, 'fruit': 0, 'fruitLiquor': 0, 'pottery': 0, 'waterSupply': 0, 'milk': 0, 'animalDeity': 0};
    // const resSave = {'workforce': 0, 'wood': 1712783159505, 'stone': 491437951039, 'food': 3649603787418, 'holy': 0, 'belief': 0, 'woodenTools': 170857312, 'reed': 10577077, 'string': 814328547, 'animalPelt': 306726908, 'animalBone': 1449498713, 'basket': 836788, 'straw': 236443996, 'rawMeat': 814755699, 'stoneTools': 269923697, 'charcoal': 6068552, 'clay': 199469853, 'ochre': 16039197, 'woodenSpear': 40, 'obsidian': 3419612, 'mineralPigments': 4194595, 'neolithicArt': 0, 'bark': 98049441, 'tar': 2105379, 'peltClothing': 1402, 'preciousStone': 0, 'neolithicCulture': 0, 'rawHide': 439604, 'stoneSpear': 17976, 'bow': 0, 'arrow': 0, 'grain': 0, 'flour': 0, 'copperOre': 67538, 'copper': 10922, 'copperTools': 10377, 'chalcolithicCulture': 0, 'vine': 13, 'fruit': 0, 'fruitLiquor': 0, 'pottery': 0, 'waterSupply': 0, 'milk': 0, 'animalDeity': 0};
    // const resSave = {'workforce': 0, 'wood': 678009965, 'stone': 488009965, 'food': 1399784421, 'holy': 0, 'belief': 0, 'woodenTools': 932284, 'reed': 39777, 'string': 894833, 'animalPelt': 76998, 'animalBone': 386872, 'basket': 2631, 'straw': 222212, 'rawMeat': 235903, 'stoneTools': 316482, 'charcoal': 0, 'clay': 0, 'ochre': 0, 'woodenSpear': 285, 'obsidian': 3878, 'mineralPigments': 3878, 'neolithicArt': 0, 'bark': 26836, 'tar': 0, 'peltClothing': 0, 'preciousStone': 0, 'neolithicCulture': 0, 'stoneSpear': 0, 'bow': 0, 'arrow': 0, 'grain': 0, 'flour': 0, 'rawHide': 0, 'vine': 0, 'copperOre': 0, 'copper': 0, 'copperTools': 0, 'chalcolithicCulture': 0, 'fruit': 0, 'fruitLiquor': 0, 'pottery': 0, 'waterSupply': 0, 'milk': 0, 'animalDeity': 0};
    this.store = Object.keys(res).reduce((result, resName) => {
      result[resName] = 0; // + Math.pow(1232123, 4);
      if (location.hash === '#abc') {
        result[resName] = resSave[resName] || 0;
      }
      if (location.hash.indexOf('#wood:') === 0) {
        const woodAmt = parseInt(location.hash.split(':')[1]);
        result[resName] = {'wood': woodAmt}[resName] || 0;
      }
      return result;
    }, {});
  }

  initNet() {
    this.net = this._getResNameIndexedObj();
    this.netPerBuildingClass = this._getResNameIndexedObj({});
  }

  initProduced() {
    this.produced = this._getResNameIndexedObj();
  }

  initDeficit() {
    this.deficit = this._getResNameIndexedObj(false);
  }

  resetNet() {
    Object.keys(this.net).forEach((resName) => {
      this.net[resName] = 0;
    });
    Object.keys(this.netPerBuildingClass).forEach((resName) => {
      this.netPerBuildingClass[resName] = {};
    });
  }

  resetProduced() {
    Object.keys(this.produced).forEach((resName) => {
      this.produced[resName] = 0;
    });
  }

  resetDeficit() {
    Object.keys(this.deficit).forEach((resName) => {
      this.deficit[resName] = false;
    });
  }

  _addNet(resName, resAmt) {
    this.net[resName] += resAmt;
  }

  _addNetPerBuildingClass(resName, resAmt, buildingClass) {
    const resNetPerBuildingClass = this.netPerBuildingClass[resName];
    if (!resNetPerBuildingClass[buildingClass]) {
      resNetPerBuildingClass[buildingClass] = 0;
    }
    resNetPerBuildingClass[buildingClass] += resAmt;
  }

  _addProduced(resName, resAmt) {
    if (resAmt > 0) {
      this.produced[resName] += resAmt;
    }
  }

  _addStore(resName, resAmt) {
    this.store[resName] += resAmt;
  }

  recalcResNet() {
    this.resetNet();
    this.resetProduced();
    this._recalcBuildings();
    this.eventBus.$emit('netUpdated');
  }

  _recalcBuildings() {
    this.buildingProvider.buildings.forEach((building) => {
      if (building.disabledByInsufficientRes) {
        return;
      }
      const buildingResNet = building.getResNet(this.exp, this.blockMap);
      Object.entries(buildingResNet).forEach(([resName, resAmt]) => {
        this._addNet(resName, resAmt);
        this._addNetPerBuildingClass(resName, resAmt, building.constructor.className);
        this._addProduced(resName, resAmt);
      });
    });
  }

  tickResStore() {
    this.resetNet();
    this.resetDeficit();
    this.resetProduced();
    this._tickBuildings();
    this.eventBus.$emit('netUpdated');
  }

  _tickBuildings() {
    this.buildingProvider.buildings.forEach((building) => {
      const buildingResNet = building.getResNet(this.exp, this.blockMap);
      if (!building.hasSufficientInputRes(buildingResNet, this)) {
        return false;
      }
      Object.entries(buildingResNet).forEach(([resName, resAmt]) => {
        this._addStore(resName, resAmt);
        this._addNet(resName, resAmt);
        this._addNetPerBuildingClass(resName, resAmt, building.constructor.className);
        this._addProduced(resName, resAmt);
      });
    });
  }

  markResDeficit(resName) {
    this.deficit[resName] = true;
  }

  serialize() {
    return {
      store: this.store,
    };
  }

  load(resStoreSave) {
    for (const resName in resStoreSave.store) {
      this.store[resName] = resStoreSave.store[resName];
    }
  }
}
