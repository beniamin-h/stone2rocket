import { decrypt, encrypt } from '@/utils/crypto';

const GAME_STATE_VERSION = 2;

export function initAutoSave(game) {
  game.$on('ticked', () => {
    saveToLocalStorage(game);
  });
}

export function getGameSave(game) {
  const gameState = game.serialize();
  gameState.version = GAME_STATE_VERSION;
  return encrypt(JSON.stringify(gameState));
}

export function saveToLocalStorage(game) {
  localStorage.setItem('gameSave', getGameSave(game));
}

export function loadFromLocalStorage(game) {
  const gameSaveData = localStorage.getItem('gameSave');
  if (!loadGameSave(game, gameSaveData)) {
    localStorage.removeItem('gameSave');
    return false;
  }
  return true;
}

export function loadGameSave(game, gameSaveData) {
  try {
    if (!gameSaveData) {
      return false;
    }
    const gameState = JSON.parse(decrypt(gameSaveData));
    if (!gameState) {
      return false;
    }
    checkVersion(gameState);
    game.load(gameState);
  } catch (e) {
    localStorage.setItem('invalidGameSave_' + Date.now(), gameSaveData);
    console.error('Cannot load gave save: ', e);
    return false;
  }
  return true;
}

function checkVersion(gameState) {
  if (gameState.version === 1) {
    gameState.stats = gameState.stats || { turnsByPhase: {} };
    gameState.version = 2;
  }
  if (gameState.version !== GAME_STATE_VERSION) {
    throw new Error('Invalid save version.');
  }
}

export function reset(game) {
  game.paused = true;
  localStorage.removeItem('gameSave');
  localStorage.setItem('oldGameSave_' + Date.now(), JSON.stringify(game.serialize()));
  window.location.reload();
}
