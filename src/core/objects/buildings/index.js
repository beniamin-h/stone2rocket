import paleolith from './paleolith';
import neolith from './neolith';
import chalcolith from './chalcolith';
import {prettyCase, dashCase} from '@/utils/stringUtils';

const buildingList = [...paleolith, ...neolith, ...chalcolith];
buildingList.forEach((buildingClass) => {
  buildingClass.displayName = buildingClass.displayName
    ? buildingClass.displayName
    : prettyCase(buildingClass.className);
  buildingClass.codeName = buildingClass.codeName
    ? buildingClass.codeName
    : dashCase(buildingClass.className);
});

export const buildings = buildingList.reduce((result, buildingClass) => {
  return {...result, [buildingClass.className]: buildingClass};
}, {});

export function getConstructibleBuildings() {
  return buildingList.filter((buildingClass) => buildingClass.constructible);
}

export function getResConsumers(resName, exp) {
  return buildingList.filter((building) => {
    return building.getResNet(1, exp)[resName] < 0;
  });
}

export function getResProducers(resName, exp) {
  return buildingList.filter((building) => {
    return building.getResNet(1, exp)[resName] > 0 ||
      building.buildGain[resName] > 0 ||
      building.upgradeGain[resName] > 0;
  });
}

export const buildingsByPhase = buildingList.reduce((result, buildingClass) => {
  return {
    ...result,
    [buildingClass.developmentPhase]: [
      ...(result[buildingClass.developmentPhase] || []),
      buildingClass
    ],
  };
}, {});

export const buildingValues = buildingList
  .reduce((result, buildingClass, index) => {
    result[buildingClass.className] = Math.pow(1.2, index);
    return result;
  }, {});

export default buildingList;
