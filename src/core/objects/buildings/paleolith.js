import Building from '@/core/objects/buildings/_base';
import { noIncrease, constIncrease, incIncrease, expIncrease } from '@/utils/increaseFunc';
import {
  stonePhase,
  believersPhase,
  collectorsPhase,
  settlementPhase,
} from '@/core/objects/buildings/developmentPhases';

export default [
  class Settlement extends Building {
    static className = 'Settlement';
    static description = 'The heart of your civilization. Upgrade it ' +
      'to make space for a new workforce.';
    static buildGain = {
      workforce: 4,
    };
    static resNet = {
      food: -1,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 1,
      stone: 1,
      food: 1,
    };
    static upgradeCostIncrease = 4;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeGain = {
      workforce: 1,
    };
    static upgradeGainIncrease = 1;
    static upgradeGainIncreaseFunc = noIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static constructible = false;
    static destructible = false;
    static developmentPhase = settlementPhase;

    static getCustomAddedResNet() {
      return { food: 1 };
    }

    getSettlementDistance(blockMap) {
      return null;
    }

    build(buildingProvider, resStore, blockMap, forFree = false) {
      const buildResult = super.build(buildingProvider, resStore, blockMap, forFree);
      if (buildResult) {
        blockMap.setSettlementCoords(this.mapX, this.mapY);
      }
      return buildResult;
    }
  },

  class WoodCollector extends Building {
    static className = 'WoodCollector';
    static description = 'The most basic source of wood.';
    static buildCost = {
      workforce: 1,
    };
    static resNet = {
      wood: 1,
    };
    static upgradable = true;
    static upgradeCost = {
      stone: 5,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = collectorsPhase;
  },

  class StoneCollector extends Building {
    static className = 'StoneCollector';
    static description = 'They collect heavy stones, but need more workforce.';
    static buildCost = {
      workforce: 2,
    };
    static resNet = {
      stone: 2,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 10,
      food: 2,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = collectorsPhase;
  },

  class FoodCollector extends Building {
    static className = 'FoodCollector';
    static description = 'That\'s where the food comes from.';
    static buildCost = {
      workforce: 1,
    };
    static resNet = {
      food: 1,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 1,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = collectorsPhase;
  },

  class Encampment extends Building {
    static className = 'Encampment';
    static description = 'A living place for your workforce. Heavily food consuming.';
    static buildCost = {
      wood: 2000,
      food: 1000,
    };
    static buildCostIncrease = 1.25;
    static buildCostIncreaseFunc = expIncrease;
    static buildGain = {
      workforce: 1,
    };
    static resNet = {
      food: -50,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 1_000,
      food: 20_000,
      woodenTools: 1,
      string: 0.5,
    };
    static upgradeCostIncrease = 1000;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeGain = {
      workforce: 1,
    };
    static upgradeGainIncrease = 1;
    static upgradeGainIncreaseFunc = noIncrease;
    static upgradeNetIncrease = 100;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = collectorsPhase;
  },

  class SacredPlace extends Building {
    static className = 'SacredPlace';
    static description = 'Some unnatural presence is here.';
    static buildCost = {
      wood: 10,
      stone: 10,
      food: 10,
    };
    static buildCostIncrease = 3;
    static buildCostIncreaseFunc = expIncrease;
    static buildGain = {
      holy: 1,
    };
    static resNet = {
      food: -20,
    };
    static developmentPhase = collectorsPhase;
  },

  class Altar extends Building {
    static className = 'Altar';
    static description = 'To soothe the gods and ward the evil away.';
    static buildCost = {
      wood: 100,
      stone: 20000,
      food: 4000,
      holy: 3,
    };
    static buildCostIncrease = 2;
    static buildCostIncreaseFunc = expIncrease;
    static buildGain = {
      belief: 3,
    };
    static resNet = {
      food: -100,
    };
    static upgradable = true;
    static upgradeCost = {
      food: 500,
    };
    static upgradeCostIncrease = 3;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeGain = {
      belief: 1,
    };
    static upgradeGainIncrease = 1;
    static upgradeGainIncreaseFunc = noIncrease;
    static upgradeNetIncrease = 1.5;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = collectorsPhase;
  },

  class WoodenToolMaker extends Building {
    static className = 'WoodenToolMaker';
    static description = 'Your people\'s first step of progress.';
    static buildCost = {
      wood: 20_000,
      stone: 1000,
      workforce: 1,
      belief: 1,
    };
    static resNet = {
      woodenTools: 1,
      wood: -200,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 40000,
      stone: 1000,
      string: 50,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = believersPhase;
  },

  class ReedCollector extends Building {
    static className = 'ReedCollector';
    static description = 'A sustained source of flexible reed.';
    static buildCost = {
      wood: 10_000,
      woodenTools: 20,
      workforce: 1,
      belief: 1,
    };
    static resNet = {
      reed: 1,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 50_000,
      woodenTools: 250,
    };
    static upgradeCostIncrease = 5;
    static upgradeCostIncreaseFunc = incIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = believersPhase;
  },

  class ReedStringMaker extends Building {
    static className = 'ReedStringMaker';
    static description = 'Process raw plants into a useful material.';
    static buildCost = {
      wood: 100_000,
      stone: 10_000,
      woodenTools: 200,
      workforce: 1,
      belief: 2,
    };
    static resNet = {
      string: 1,
      reed: -2,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 100_000,
      stone: 5000,
      woodenTools: 200,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = believersPhase;
  },

  class WoodGatherer extends Building {
    static className = 'WoodGatherer';
    static description = 'Notably boost your wood yield using handy strings.';
    static buildCost = {
      wood: 10_000,
      string: 250,
      workforce: 1,
      belief: 1,
    };
    static resNet = {
      wood: 4096,
      string: -2,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 2000,
      woodenTools: 100,
      string: 500,
    };
    static upgradeCostIncrease = 4;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = believersPhase;
  },

  class Huntsman extends Building {
    static className = 'Huntsman';
    static description = 'Using some wooden tools he provides a small amount of \n' +
      'basic animal resources for your further development.';
    static buildCost = {
      wood: 50_000,
      woodenTools: 500,
      workforce: 1,
    };
    static resNet = {
      food: 128,
      animalPelt: 1,
      animalBone: 1,
      woodenTools: -2,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 25_000,
      woodenTools: 150,
      string: 100,
    };
    static upgradeCostIncrease = 3;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = constIncrease;
    static developmentPhase = believersPhase;
  },

  class BasketMaker extends Building {
    static className = 'BasketMaker';
    static description = 'Weaving reeds into simple baskets allows your people \n' +
      'to gather much more resources.';
    static buildCost = {
      wood: 250_000,
      woodenTools: 1250,
      animalBone: 200,
      workforce: 1,
      belief: 1,
    };
    static resNet = {
      basket: 1,
      reed: -8,
      string: -2,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 150_000,
      woodenTools: 250,
      string: 100,
      animalBone: 50,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1.5;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = believersPhase;
  },

  class StrawCollector extends Building {
    static className = 'StrawCollector';
    static description = 'Straw is an essential building material stored in a bale.';
    static buildCost = {
      wood: 250_000,
      string: 2000,
      workforce: 1,
      belief: 1,
    };
    static resNet = {
      straw: 1,
      string: -2,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 250_000,
      stoneTools: 50,
      string: 250,
    };
    static upgradeCostIncrease = 1.5;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 3;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = believersPhase;
  },

  class FoodGatherer extends Building {
    static className = 'FoodGatherer';
    static description = 'These baskets are really helpful.';
    static buildCost = {
      wood: 350_000,
      woodenTools: 1500,
      straw: 150,
      workforce: 1,
      belief: 1,
    };
    static buildCostIncrease = 1.2;
    static buildCostIncreaseFunc = expIncrease;
    static resNet = {
      food: 6_000,
      basket: -1,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 25_000,
      woodenTools: 3000,
      straw: 25,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = believersPhase;
  },

  class FishCatcher extends Building {
    static className = 'FishCatcher';
    static description = 'Yeah. It\'s gonna take some time to catch them.';
    static buildCost = {
      wood: 1_000_000,
      straw: 2000,
      woodenTools: 1000,
      string: 2000,
      workforce: 1,
      belief: 1,
    };
    static resNet = {
      food: 32_000,
      rawMeat: 6,
      woodenTools: -1,
      string: -1,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 2_000_000,
      woodenTools: 1000,
      string: 2500,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = believersPhase;
  },

  class CultHut extends Building {
    static className = 'CultHut';
    static description = 'It\'s clear they definitely know what they are doing.';
    static buildCost = {
      wood: 500_000,
      stone: 10_000,
      straw: 250,
      woodenTools: 500,
      workforce: 1,
    };
    static buildGain = {
      belief: 1,
    };
    static resNet = {
      animalBone: -4,
      animalPelt: -1,
      food: -1000,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 200_000,
      stone: 20_000,
      straw: 1000,
      woodenTools: 750,
      animalBone: 2000,
    };
    static upgradeCostIncrease = 1;
    static upgradeCostIncreaseFunc = incIncrease;
    static upgradeGain = {
      holy: 1,
    };
    static upgradeGainIncrease = 1;
    static upgradeGainIncreaseFunc = noIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = believersPhase;
  },

  class StoneToolMaker extends Building {
    static className = 'StoneToolMaker';
    static description = 'You\'re going to need some stones.';
    static buildCost = {
      wood: 500_000,
      stone: 1_000_000,
      woodenTools: 50_000,
      workforce: 1,
      belief: 3,
    };
    static resNet = {
      stoneTools: 1,
      stone: -2000,
    };
    static getCustomAddedResNet() {
      return { stoneTools: 3 };
    }
    static upgradable = true;
    static upgradeCost = {
      wood: 100_000,
      stone: 500_000,
      woodenTools: 25_000,
      string: 1000,
      stoneTools: 50,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 0.49;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = stonePhase;
  },

  class ScavengerPost extends Building {
    static className = 'ScavengerPost';
    static description = 'It doesn\'t stink anymore.';
    static buildCost = {
      wood: 300_000,
      stoneTools: 2000,
      straw: 100,
      workforce: 1,
    };
    static resNet = {
      animalPelt: 4,
      animalBone: 12,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 200_000,
      stoneTools: 2000,
      straw: 500,
    };
    static upgradeCostIncrease = 1;
    static upgradeCostIncreaseFunc = constIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = constIncrease;
    static developmentPhase = stonePhase;
  },

  class ClayPit extends Building {
    static className = 'ClayPit';
    static description = 'A hole in the ground full of precious mud.';
    static buildCost = {
      wood: 10_000_000,
      woodenTools: 10_000,
      stoneTools: 3_000,
      workforce: 2,
      belief: 1,
    };
    static resNet = {
      clay: 1,
      ochre: 1,
      basket: -2,
      woodenTools: -4,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 12_000_000,
      woodenTools: 10_000,
      stoneTools: 1500,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = stonePhase;
  },

  class EarthOven extends Building {
    static className = 'EarthOven';
    static description = 'The easiest way to get a warm meal.';
    static buildCost = {
      clay: 3500,
      straw: 25_000,
      stone: 500_000,
      stoneTools: 1500,
      workforce: 1,
      belief: 1,
    };
    static resNet = {
      food: 200_000,
      charcoal: 1,
      rawMeat: -320,
      wood: -2048,
    };
    static upgradable = true;
    static upgradeCost = {
      clay: 1000,
      straw: 750,
      stone: 200_000,
      stoneTools: 500,
    };
    static upgradeCostIncrease = 3;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = stonePhase;
  },

  class WoodenSpearMaker extends Building {
    static className = 'WoodenSpearMaker';
    static description = 'The sharpened end provides for greater ease in driving it in.';
    static buildCost = {
      wood: 2_500_000,
      stoneTools: 7500,
      workforce: 1,
      belief: 1,
    };
    static resNet = {
      woodenSpear: 1,
      wood: -5000,
      stoneTools: -2,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 5_000_000,
      stoneTools: 10_000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = stonePhase;
  },

  class WoodenSpearHunter extends Building {
    static className = 'WoodenSpearHunter';
    static description = 'A more reliable source of fresh animal resources.';
    static buildCost = {
      wood: 5_000_000,
      straw: 2000,
      stoneTools: 5000,
      workforce: 1,
    };
    static resNet = {
      rawMeat: 160,
      animalBone: 120,
      animalPelt: 60,
      woodenSpear: -2,
      stoneTools: -1,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 25_000_000,
      straw: 3000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = stonePhase;
  },

  class StonePickPit extends Building {
    static className = 'StonePickPit';
    static description = 'Do you need more stone?';
    static buildCost = {
      wood: 20_000_000,
      stoneTools: 20_000,
      workforce: 2,
      belief: 2,
    };
    static resNet = {
      stone: 64_000,
      obsidian: 1,
      mineralPigments: 1,
      basket: -1,
      stoneTools: -3,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 25_000_000,
      stoneTools: 20_000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = stonePhase;
  },

  class StoneAxePost extends Building {
    static className = 'StoneAxePost';
    static description = 'Stone tools make wood production much easier \n' +
      '- and provide important tree bark.';
    static buildCost = {
      stone: 7_500_000,
      straw: 12_000,
      string: 20_000,
      stoneTools: 12_000,
      workforce: 1,
      belief: 1,
    };
    static resNet = {
      wood: 128_000,
      bark: 2,
      stoneTools: -1,
    };
    static upgradable = true;
    static upgradeCost = {
      stone: 15_000_000,
      straw: 20_000,
      string: 20_000,
      stoneTools: 15_000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = stonePhase;
  },

  class PeltStringMaker extends Building {
    static className = 'PeltStringMaker';
    static description = 'Make some use of these pelts.';
    static buildCost = {
      wood: 500_000,
      straw: 2000,
      stoneTools: 2000,
      workforce: 1,
      belief: 1,
    };
    static resNet = {
      string: 128,
      animalPelt: -20,
      woodenTools: -1,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 500_000,
      straw: 4000,
      stoneTools: 1500,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = stonePhase;
  },

  class BarkStringMaker extends Building {
    static className = 'BarkStringMaker';
    static description = 'Strings, strings, strings.';
    static buildCost = {
      stone: 8_000_000,
      wood: 10_000_000,
      straw: 25_000,
      stoneTools: 25_000,
      workforce: 1,
      belief: 1,
    };
    static resNet = {
      string: 2000,
      stoneTools: -1,
      bark: -1,
    };
    static upgradable = true;
    static upgradeCost = {
      stone: 25_000_000,
      wood: 25_000_000,
      straw: 75_000,
      stoneTools: 50_000,
    };
    static upgradeCostIncrease = 4;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = stonePhase;
  },

  class WoodenToolSharpener extends Building {
    static className = 'WoodenToolSharpener';
    static description = 'By using a few stone tools we can provide a great number of wooden tools.';
    static buildCost = {
      wood: 20_000_000,
      stone: 20_000_000,
      straw: 20_000,
      woodenTools: 20_000,
      stoneTools: 5000,
      workforce: 1,
      belief: 1,
    };
    static resNet = {
      woodenTools: 64,
      stoneTools: -1,
      stone: -1000,
      wood: -25_000,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 30_000_000,
      stone: 20_000_000,
      stoneTools: 4000,
    };
    static upgradeCostIncrease = 3;
    static upgradeCostIncreaseFunc = incIncrease;
    static upgradeNetIncrease = 1.5;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = stonePhase;
  },

];
