import Building from '@/core/objects/buildings/_base';
import { noIncrease, constIncrease, incIncrease, expIncrease } from '@/utils/increaseFunc';
import {
  neolithicArtistsPhase,
  lateNeolithicArtistsPhase,
  neolithicFirePhase,
  lateNeolithicStonePhase,
  lateNeolithicCopperPhase,
} from '@/core/objects/buildings/developmentPhases';

export default [
  class ObsidianBeadMaker extends Building {
    static className = 'ObsidianBeadMaker';
    static description = 'A shiny black stone on a string.';
    static buildCost = {
      wood: 20_000_000,
      straw: 20_000,
      stoneTools: 15_000,
      belief: 1,
    };
    static buildCostIncrease = 5;
    static buildCostIncreaseFunc = expIncrease;
    static buildGain = {
      neolithicArt: 2,
    };
    static resNet = {
      obsidian: -2,
      stoneTools: -1,
      string: -1,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 25_000_000,
      straw: 20_000,
      stoneTools: 20_000,
      stone: 10_000_000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeGain = {
      neolithicArt: 1,
    };
    static upgradeGainIncrease = 1;
    static upgradeGainIncreaseFunc = noIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = neolithicArtistsPhase;
  },

  class ClayFigurineMaker extends Building {
    static className = 'ClayFigurineMaker';
    static description = 'Everyone wants these little figurines.';
    static buildCost = {
      wood: 20_000_000,
      straw: 20_000,
      clay: 10_000,
      woodenTools: 50_000,
      belief: 1,
    };
    static buildCostIncrease = 5;
    static buildCostIncreaseFunc = expIncrease;
    static buildGain = {
      neolithicArt: 1,
    };
    static resNet = {
      clay: -2,
      wood: -2_000,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 40_000_000,
      straw: 40_000,
      woodenTools: 45_000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeGain = {
      neolithicArt: 1,
    };
    static upgradeGainIncrease = 1;
    static upgradeGainIncreaseFunc = noIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = neolithicArtistsPhase;
  },

  class BoneCarver extends Building {
    static className = 'BoneCarver';
    static description = 'An art from bones.';
    static buildCost = {
      wood: 20_000_000,
      stone: 15_000_000,
      straw: 20_000,
      stoneTools: 12_000,
      belief: 1,
    };
    static buildCostIncrease = 5;
    static buildCostIncreaseFunc = expIncrease;
    static buildGain = {
      neolithicArt: 1,
    };
    static resNet = {
      animalBone: -20,
      stoneTools: -1,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 20_000_000,
      stone: 20_000_000,
      straw: 50_000,
      stoneTools: 20_000,
    };
    static upgradeCostIncrease = 3;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeGain = {
      neolithicArt: 1,
    };
    static upgradeGainIncrease = 1;
    static upgradeGainIncreaseFunc = noIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = neolithicArtistsPhase;
  },

  class TarPit extends Building {
    static className = 'TarPit';
    static description = 'A drowning pit.';
    static buildCost = {
      wood: 15_000_000,
      stone: 2_000_000,
      stoneTools: 5_000,
      workforce: 2,
      belief: 1,
    };
    static resNet = {
      tar: 1,
      animalPelt: -50,
      string: -12,
      stoneTools: -2,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 10_000_000,
      stone: 3_000_000,
      stoneTools: 2_000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = neolithicArtistsPhase;
  },

  class TarPainter extends Building {
    static className = 'TarPainter';
    static description = 'You will be black.';
    static buildCost = {
      wood: 20_000_000,
      straw: 150_000,
      woodenTools: 27_500,
      belief: 1,
    };
    static buildCostIncrease = 5;
    static buildCostIncreaseFunc = expIncrease;
    static buildGain = {
      neolithicArt: 1,
    };
    static resNet = {
      animalPelt: -10,
      tar: -4,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 20_000_000,
      stone: 20_000_000,
      straw: 20_000,
      animalBone: 20_000,
      woodenTools: 20_000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeGain = {
      neolithicArt: 1,
    };
    static upgradeGainIncrease = 1;
    static upgradeGainIncreaseFunc = noIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = neolithicArtistsPhase;
  },

  class OchrePainter extends Building {
    static className = 'OchrePainter';
    static description = 'The red is not passé.';
    static buildCost = {
      wood: 30_000_000,
      stone: 40_000_000,
      straw: 50_000,
      stoneTools: 30_000,
      animalPelt: 2000,
      belief: 1,
    };
    static buildCostIncrease = 5;
    static buildCostIncreaseFunc = expIncrease;
    static buildGain = {
      neolithicArt: 1,
    };
    static resNet = {
      animalPelt: -1,
      ochre: -10,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 20_000_000,
      stone: 25_000_000,
      straw: 15_000,
      woodenTools: 20_000,
      animalPelt: 2000,
    };
    static upgradeCostIncrease = 1;
    static upgradeCostIncreaseFunc = incIncrease;
    static upgradeGain = {
      neolithicArt: 1,
    };
    static upgradeGainIncrease = 1;
    static upgradeGainIncreaseFunc = noIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = neolithicArtistsPhase;
  },

  class CharcoalPainter extends Building {
    static className = 'CharcoalPainter';
    static description = 'Please don\'t close to fire.';
    static buildCost = {
      wood: 75_000_000,
      stone: 20_000_000,
      straw: 125_000,
      woodenTools: 150_000,
      animalPelt: 24_000,
      belief: 1,
    };
    static buildCostIncrease = 5;
    static buildCostIncreaseFunc = expIncrease;
    static buildGain = {
      neolithicArt: 1,
    };
    static resNet = {
      animalPelt: -16,
      charcoal: -2,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 150_000_000,
      stone: 20_000_000,
      straw: 200_000,
      woodenTools: 75_000,
      animalPelt: 8000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeGain = {
      neolithicArt: 1,
    };
    static upgradeGainIncrease = 1;
    static upgradeGainIncreaseFunc = noIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = neolithicArtistsPhase;
  },

  class CavePainting extends Building {
    static className = 'CavePainting';
    static description = 'Made by the finest artists.';
    static buildCost = {
      tar: 2000,
      ochre: 2000,
      charcoal: 2000,
      mineralPigments: 2000,
      bark: 6000,
      animalPelt: 10_000,
      stoneTools: 7500,
    };
    static buildCostIncrease = 2;
    static buildCostIncreaseFunc = expIncrease;
    static buildGain = {
      neolithicArt: 1,
    };
    static resNet = {};
    static upgradable = true;
    static upgradeCost = {
      tar: 20_000,
      ochre: 20_000,
      charcoal: 20_000,
      mineralPigments: 20_000,
      bark: 50_000,
      animalPelt: 50_000,
      stoneTools: 50_000,
      wood: 50_000_000,
    };
    static upgradeCostIncrease = 10;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeGain = {
      neolithicArt: 1,
      belief: 1,
      holy: 1,
    };
    static upgradeGainIncrease = 1;
    static upgradeGainIncreaseFunc = constIncrease;
    static developmentPhase = neolithicArtistsPhase;
  },

  class PeltSewer extends Building {
    static className = 'PeltSewer';
    static description = 'Finally something stylish to wear and warm.';
    static buildCost = {
      wood: 100_000_000,
      straw: 35_000,
      woodenTools: 200_000,
      stoneTools: 50_000,
      animalBone: 20_000,
      workforce: 1,
      belief: 1,
    };
    static resNet = {
      peltClothing: 1,
      animalPelt: -500,
      animalBone: -2,
      string: -10,
      woodenTools: -16,
      stoneTools: -2,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 33_000_000,
      straw: 25_000,
      stoneTools: 30_000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = neolithicFirePhase;
  },

  class FirePit extends Building {
    static className = 'FirePit';
    static description = 'Let\'s make a big fire for the people.';
    static buildCost = {
      wood: 1_000_000_000,
      stone: 100_000_000,
      straw: 20_000,
      stoneTools: 50_000,
      neolithicArt: 16,
    };
    static buildCostIncrease = 1.5;
    static buildCostIncreaseFunc = expIncrease;
    static buildGain = {
      neolithicCulture: 10,
    };
    static resNet = {
      wood: -4_000_000,
      animalBone: -200,
      stoneTools: -5,
    };
    static upgradable = true;
    static upgradeCost = {
      neolithicArt: 2,
      wood: 500_000_000,
      stone: 150_000_000,
      straw: 100_000,
      stoneTools: 25_000,
    };
    static upgradeCostIncrease = 1;
    static upgradeCostIncreaseFunc = incIncrease;
    static upgradeGain = {
      neolithicCulture: 2,
    };
    static upgradeGainIncrease = 1;
    static upgradeGainIncreaseFunc = noIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = neolithicFirePhase;
  },

  class TentCamp extends Building {
    static className = 'TentCamp';
    static description = 'A portable shelter made from animal pelts, wooden sticks and strings.';
    static buildCost = {
      wood: 50_000_000,
      stone: 2_000_000,
      animalPelt: 50_000,
      animalBone: 20_000,
      string: 50_000,
      stoneTools: 20_000,
      peltClothing: 100,
      neolithicCulture: 1,
    };
    static buildCostIncrease = 1.2;
    static buildCostIncreaseFunc = expIncrease;
    static buildGain = {
      workforce: 10,
    };
    static resNet = {
      food: -200_000_000,
      wood: -100_000,
      peltClothing: -1,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 25_000_000,
      animalPelt: 5_000,
      animalBone: 2_000,
      string: 2_000,
      stoneTools: 1_000,
    };
    static upgradeCostIncrease = 1.5;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeGain = {
      workforce: 2,
    };
    static upgradeGainIncrease = 1;
    static upgradeGainIncreaseFunc = noIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = neolithicFirePhase;
  },

  class StoneToolSharpener extends Building {
    static className = 'StoneToolSharpener';
    static description = 'Obsidian appears to be perfect to produce very sharp stone tools.';
    static buildCost = {
      wood: 150_000_000,
      stone: 300_000_000,
      straw: 160_000,
      stoneTools: 200_000,
      animalPelt: 60_000,
      animalBone: 60_000,
      workforce: 4,
      neolithicCulture: 1,
    };
    static resNet = {
      stoneTools: 256,
      obsidian: -1,
      string: -50,
      stone: -50_000,
      wood: -10_000,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 50_000_000,
      stone: 360_000_000,
      straw: 50_000,
      stoneTools: 7_500,
      rawHide: 250,
    };
    static upgradeCostIncrease = 1.5;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1.5;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = lateNeolithicStonePhase;
  },

  class StoneSpearMaker extends Building {
    static className = 'StoneSpearMaker';
    static description = 'Attaching a sharp stone at the end of a stick makes a real difference.';
    static buildCost = {
      wood: 175_000_000,
      stone: 150_000_000,
      straw: 75_000,
      stoneTools: 150_000,
      workforce: 2,
      neolithicCulture: 1,
    };
    static resNet = {
      stoneSpear: 1,
      wood: -40_000,
      stone: -20_000,
      string: -50,
      stoneTools: -8,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 200_000_000,
      stone: 250_000_000,
      straw: 50_000,
      stoneTools: 30_000,
    };
    static upgradeCostIncrease = 3.5;
    static upgradeCostIncreaseFunc = incIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = lateNeolithicStonePhase;
  },

  class StoneSpearHunter extends Building {
    static className = 'StoneSpearHunter';
    static description = 'Now even the largest animals will not threaten us.';
    static buildCost = {
      wood: 150_000_000,
      straw: 60_000,
      stoneTools: 50_000,
      workforce: 3,
      neolithicCulture: 1,
    };
    static resNet = {
      rawMeat: 5120,
      animalBone: 1000,
      animalPelt: 750,
      stoneSpear: -1,
      string: -2,
      stoneTools: -2,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 30_000_000,
      straw: 20_000,
      stoneTools: 50_000,
    };
    static upgradeCostIncrease = 3;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = lateNeolithicStonePhase;
  },

  class CharcoalPile extends Building {
    static className = 'CharcoalPile';
    static description = 'Make a big wood pile, cover it with dirt, set it on fire \n' +
      'and come the next day.';
    static buildCost = {
      stone: 10_000_000,
      wood: 20 * 10 ** 9,
      straw: 20_000,
      neolithicCulture: 1,
    };
    static resNet = {
      charcoal: 1000,
      basket: -64,
      wood: -16_000_000,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 100 * 10 ** 9,
    };
    static upgradeCostIncrease = 2.5;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = lateNeolithicStonePhase;
  },

  class FoodGrindingHut extends Building {
    static className = 'FoodGrindingHut';
    static description = 'Smash it!';
    static buildCost = {
      stone: 200_000_000,
      wood: 20_000_000,
      straw: 20_000,
      stoneTools: 200_000,
      workforce: 4,
      neolithicCulture: 1,
    };
    static resNet = {
      food: 150_000_000,
      stone: -6_000_000,
      stoneTools: -2_000,
    };
    static upgradable = true;
    static upgradeCost = {
      stone: 150_000_000,
      wood: 20_000_000,
      straw: 20_000,
      stoneTools: 150_000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = lateNeolithicStonePhase;
  },

  class SmokeHut extends Building {
    static className = 'SmokeHut';
    static description = 'The best way to preserve your meat.';
    static buildCost = {
      wood: 1_600_000_000,
      stone: 800_000_000,
      straw: 200_000,
      string: 100_000,
      stoneTools: 400_000,
      rawHide: 2_000,
      workforce: 4,
      neolithicCulture: 1,
    };
    static resNet = {
      food: 5_000_000_000,
      wood: -8_000_000,
      straw: -50_000,
      string: -20_000,
      rawMeat: -80_000,
      charcoal: -1000,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 4_000_000_000,
      string: 100_000,
      rawHide: 10_000,
      stoneTools: 300_000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = incIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = lateNeolithicStonePhase;
  },

  class ClayExcavationSite extends Building {
    static className = 'ClayExcavationSite';
    static description = 'Digging clay with the use of stone tools.';
    static buildCost = {
      stone: 20_000_000,
      wood: 300_000_000,
      stoneTools: 200_000,
      workforce: 10,
      neolithicCulture: 2,
    };
    static resNet = {
      clay: 1_000,
      ochre: 100,
      basket: -80,
      stoneTools: -200,
    };
    static upgradable = true;
    static upgradeCost = {
      stone: 10_000_000,
      wood: 400_000_000,
      stoneTools: 200_000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = lateNeolithicStonePhase;
  },

  class VineCollector extends Building {
    static className = 'VineCollector';
    static description = 'Stems, lianas and runners.';
    static buildCost = {
      wood: 150_000_000,
      string: 25_000,
      stoneTools: 160_000,
      workforce: 6,
      neolithicCulture: 2,
    };
    static resNet = {
      vine: 1,
      stoneTools: -200,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 50_000_000,
      string: 100_000,
      stoneTools: 20_000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = lateNeolithicStonePhase;
  },

  class VineStringMaker extends Building {
    static className = 'VineStringMaker';
    static description = 'Stronger than reed, vine allows string mass production \n' +
      'in expense of a few stone tools.';
    static buildCost = {
      wood: 500_000_000,
      stoneTools: 50_000,
      workforce: 5,
      neolithicCulture: 2,
    };
    static resNet = {
      string: 100_000,
      vine: -1,
      stoneTools: -160,
      woodenTools: -250,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 250_000_000,
      string: 30_000,
      stoneTools: 30_000,
      woodenTools: 100_000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = lateNeolithicStonePhase;
  },

  class HideTanner extends Building {
    static className = 'HideTanner';
    static description = 'Your first step of pelt processing.';
    static buildCost = {
      wood: 200_000_000,
      straw: 300_000,
      stoneTools: 150_000,
      neolithicCulture: 2,
      workforce: 3,
    };
    static resNet = {
      rawHide: 1,
      wood: -75_000,
      animalPelt: -3_000,
      stoneTools: -25,
      woodenTools: -4,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 400_000_000,
      straw: 150_000,
      stoneTools: 100_000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = lateNeolithicArtistsPhase;
  },

  class BowMaker extends Building {
    static className = 'BowMaker';
    static description = 'A weapon for shooting arrows.';
    static buildCost = {
      wood: 750_000_000,
      straw: 150_000,
      stoneTools: 90_000,
      woodenTools: 1_500_000,
      workforce: 2,
      neolithicCulture: 1,
    };
    static resNet = {
      bow: 1,
      wood: -200_000,
      string: -1500,
      stoneTools: -100,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 350_000_000,
      woodenTools: 1_500_000,
      straw: 150_000,
      stoneTools: 30_000,
    };
    static upgradeCostIncrease = 2.5;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = lateNeolithicArtistsPhase;
  },

  class ArrowFletcher extends Building {
    static className = 'ArrowFletcher';
    static description = 'Make me some arrows.';
    static buildCost = {
      wood: 500_000_000,
      stone: 700_000_000,
      straw: 60_000,
      stoneTools: 200_000,
      workforce: 2,
      neolithicCulture: 1,
    };
    static resNet = {
      arrow: 10,
      wood: -300_000,
      stone: -300_000,
      string: -300,
      stoneTools: -100,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 250_000_000,
      stone: 500_000_000,
      straw: 150_000,
      stoneTools: 25_000,
    };
    static upgradeCostIncrease = 2.5;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = lateNeolithicArtistsPhase;
  },

  class BowHunter extends Building {
    static className = 'BowHunter';
    static description = 'A professional hunter with a deadly weapon.';
    static buildCost = {
      wood: 1_500_000_000,
      straw: 180_000,
      stoneTools: 450_000,
      string: 200_000,
      rawHide: 1000,
      workforce: 2,
      neolithicCulture: 1,
    };
    static resNet = {
      rawMeat: 250_000,
      animalBone: 50_000,
      animalPelt: 100_000,
      stoneTools: -250,
      basket: -100,
      arrow: -20,
      string: -16,
      bow: -1,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 500_000_000,
      stone: 200_000_000,
      straw: 150_000,
      stoneTools: 35_000,
    };
    static upgradeCostIncrease = 2.5;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = lateNeolithicArtistsPhase;
  },

  class Woodcarver extends Building {
    static className = 'Woodcarver';
    static description = 'Shape wood to create an art.';
    static buildCost = {
      wood: 500_000_000,
      stone: 150_000_000,
      straw: 500_000,
      stoneTools: 500_000,
      neolithicCulture: 1,
      workforce: 2,
      belief: 1,
    };
    static buildGain = {
      neolithicArt: 2,
    };
    static resNet = {
      wood: -500_000,
      stoneTools: -100,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 50_000_000,
      stoneTools: 50_000,
      straw: 2_000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeGain = {
      neolithicArt: 2,
    };
    static upgradeGainIncrease = 1;
    static upgradeGainIncreaseFunc = noIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = lateNeolithicArtistsPhase;
  },

  class StoneSculptor extends Building {
    static className = 'StoneSculptor';
    static description = 'A fine stone art.';
    static buildCost = {
      wood: 180_000_000,
      stone: 900_000_000,
      straw: 200_000,
      stoneTools: 600_000,
      neolithicCulture: 1,
      workforce: 2,
      belief: 1,
    };
    static buildGain = {
      neolithicArt: 2,
    };
    static resNet = {
      stone: -200_000,
      stoneTools: -160,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 35_000_000,
      stone: 100_000_000,
      straw: 1_000,
      stoneTools: 20_000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeGain = {
      neolithicArt: 2,
    };
    static upgradeGainIncrease = 1;
    static upgradeGainIncreaseFunc = noIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = lateNeolithicArtistsPhase;
  },

  class DrumMaker extends Building {
    static className = 'DrumMaker';
    static description = 'Let them hear you.';
    static buildCost = {
      wood: 500_000_000,
      straw: 200_000,
      stoneTools: 50_000,
      rawHide: 500,
      neolithicCulture: 1,
      workforce: 3,
      belief: 1,
    };
    static buildCostIncrease = 1.5;
    static buildCostIncreaseFunc = constIncrease;
    static buildGain = {
      neolithicArt: 4,
    };
    static resNet = {
      wood: -150_000,
      rawHide: -1,
      animalBone: -10,
      stoneTools: -40,
      woodenTools: -100,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 400_000_000,
      stone: 200_000_000,
      straw: 150_000,
    };
    static upgradeCostIncrease = 1.5;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeGain = {
      neolithicArt: 4,
    };
    static upgradeGainIncrease = 1;
    static upgradeGainIncreaseFunc = noIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = lateNeolithicArtistsPhase;
  },

  class BoneFluteMaker extends Building {
    static className = 'BoneFluteMaker';
    static description = 'Play the music to soothe your nerves.';
    static buildCost = {
      wood: 1_200_000_000,
      straw: 500_000,
      stoneTools: 50_000,
      neolithicCulture: 1,
      workforce: 3,
      belief: 1,
    };
    static buildCostIncrease = 2;
    static buildCostIncreaseFunc = constIncrease;
    static buildGain = {
      neolithicArt: 1,
    };
    static resNet = {
      animalBone: -40_000,
      stoneTools: -120,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 500_000_000,
      straw: 100_000,
      stoneTools: 20_000,
      rawHide: 1000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeGain = {
      neolithicArt: 1,
    };
    static upgradeGainIncrease = 1;
    static upgradeGainIncreaseFunc = constIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = lateNeolithicArtistsPhase;
  },

  class PreciousStonePit extends Building {
    static className = 'PreciousStonePit';
    static description = 'Go on, find these little stones for me.';
    static buildCost = {
      wood: 1_000_000_000,
      stone: 600_000_000,
      stoneTools: 300_000,
      neolithicCulture: 1,
      workforce: 15,
    };
    static resNet = {
      preciousStone: 1,
      food: -15_000_000,
      basket: -50,
      stoneTools: -640,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 1_500_000_000,
      stone: 1_000_000_000,
      stoneTools: 500_000,
      rawHide: 1000,
      workforce: 1,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = lateNeolithicArtistsPhase;
  },

  class PreciousStoneWorker extends Building {
    static className = 'PreciousStoneWorker';
    static description = 'It\'s so shiny.';
    static buildCost = {
      wood: 500_000_000,
      stone: 175_000_000,
      straw: 200_000,
      stoneTools: 200_000,
      neolithicCulture: 1,
      workforce: 1,
      belief: 1,
    };
    static buildCostIncrease = 2;
    static buildCostIncreaseFunc = constIncrease;
    static buildGain = {
      neolithicArt: 12,
    };
    static resNet = {
      preciousStone: -1,
      string: -128,
      stoneTools: -150,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 250_000_000,
      stone: 75_000_000,
      straw: 100_000,
      stoneTools: 100_000,
      rawHide: 2000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeGain = {
      neolithicArt: 6,
    };
    static upgradeGainIncrease = 1;
    static upgradeGainIncreaseFunc = noIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = lateNeolithicArtistsPhase;
  },

  class CopperOreGatherer extends Building {
    static className = 'CopperOreGatherer';
    static description = 'Collect your first formable metal \n' +
      'to bring your people a development breakout.';
    static buildCost = {
      wood: 200_000_000,
      stone: 400_000_000,
      stoneTools: 4_000_000,
      rawHide: 10_000,
      workforce: 25,
      neolithicCulture: 5,
    };
    static buildCostIncrease = 1.2;
    static buildCostIncreaseFunc = expIncrease;
    static resNet = {
      copperOre: 1,
      stoneTools: -512,
      basket: -100,
    };
    static getCustomAddedResNet() {
      return { copperOre: 1 };
    }
    static upgradable = true;
    static upgradeCost = {
      wood: 200_000_000,
      stone: 400_000_000,
      stoneTools: 5_000_000,
      rawHide: 40_000,
      workforce: 1,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = constIncrease;
    static developmentPhase = lateNeolithicCopperPhase;
  },

  class CopperHammeringPost extends Building {
    static className = 'CopperHammeringPost';
    static description = 'Hammer your own copper hardware.';
    static buildCost = {
      stone: 480_000_000,
      wood: 200_000_000,
      straw: 20_000,
      stoneTools: 10_000_000,
      rawHide: 20_000,
      workforce: 10,
      neolithicCulture: 6,
    };
    static resNet = {
      copperTools: 1,
      stoneTools: -2500,
      string: -120,
      copperOre: -2,
    };
    static upgradable = true;
    static upgradeCost = {
      stone: 480_000_000,
      straw: 30_000,
      stoneTools: 9_990_000,
      rawHide: 50_000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = lateNeolithicCopperPhase;
  },

  class CopperAxePost extends Building {
    static className = 'CopperAxePost';
    static description = 'Deeper strokes, lighter weight, increased tool durability. \n' +
      'In short - much more wood in a shorter time.';
    static buildCost = {
      stone: 2_000_000_000,
      wood: 150_000_000,
      straw: 4_000_000,
      string: 150_000,
      copperTools: 512,
      workforce: 10,
      neolithicCulture: 3,
    };
    static resNet = {
      wood: 160_000_000,
      bark: 1000,
      copperTools: -1,
    };
    static upgradable = true;
    static upgradeCost = {
      stone: 200_000_000,
      wood: 100_000_000,
      straw: 400_000,
      string: 150_000,
      copperTools: 128,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = lateNeolithicCopperPhase;
  },

  class CopperPickStonePit extends Building {
    static className = 'CopperPickStonePit';
    static description = 'Still need more stone?';
    static buildCost = {
      stone: 3_500_000_000,
      wood: 750_000_000,
      copperTools: 1500,
      workforce: 16,
      neolithicCulture: 4,
    };
    static resNet = {
      stone: 320_000_000,
      obsidian: 256,
      mineralPigments: 200,
      basket: -64,
      copperTools: -3,
    };
    static upgradable = true;
    static upgradeCost = {
      stone: 200_000_000,
      wood: 15_000_000,
      copperTools: 3000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = lateNeolithicCopperPhase;
  },

  class BarkTarBrewer extends Building {
    static className = 'BarkTarBrewer';
    static description = 'We can produce tar from bark.';
    static buildCost = {
      wood: 800_000_000,
      stone: 500_000_000,
      string: 70_000,
      copperTools: 200,
      workforce: 12,
      neolithicCulture: 2,
    };
    static resNet = {
      tar: 160,
      bark: -250,
      wood: -10_000_000,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 500_000_000,
      stone: 500_000_000,
      rawHide: 30_000,
      copperTools: 100,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = lateNeolithicCopperPhase;
  },

  class StrawGatherer extends Building {
    static className = 'StrawGatherer';
    static description = 'Use your finest copper tools to gather tons of straw.';
    static buildCost = {
      wood: 500_000_000,
      string: 100_000,
      copperTools: 1_000,
      workforce: 10,
      neolithicCulture: 2,
    };
    static resNet = {
      straw: 50_000,
      string: -25_000,
      copperTools: -1,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 500_000_000,
      string: 200_000,
      copperTools: 2_000,
      workforce: 1,
    };
    static upgradeCostIncrease = 1;
    static upgradeCostIncreaseFunc = incIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = constIncrease;
    static developmentPhase = lateNeolithicCopperPhase;
  },

  class WildCerealField extends Building {
    static className = 'WildCerealField';
    static description = 'A primitive field full of different cereal grain plants.';
    static buildCost = {
      wood: 650_000_000,
      stone: 20_000_000,
      stoneTools: 2_000_000,
      string: 450_000,
      workforce: 6,
      neolithicCulture: 1,
    };
    static buildCostIncrease = 1.1;
    static buildCostIncreaseFunc = expIncrease;
    static resNet = {
      grain: 1,
      basket: -32,
      stoneTools: -32,
    };
    static getCustomAddedResNet() {
      return { grain: 3 };
    }
    static upgradable = true;
    static upgradeCost = {
      wood: 500_000_000,
      stone: 20_000_000,
      stoneTools: 1_000_000,
      string: 500_000,
      workforce: 1,
    };
    static upgradeCostIncrease = 1.5;
    static upgradeCostIncreaseFunc = incIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = constIncrease;
    static developmentPhase = lateNeolithicCopperPhase;
  },

  class GrainGrindingHut extends Building {
    static className = 'GrainGrindingHut';
    static description = 'Turn firm grains into usable flour.';
    static buildCost = {
      stone: 2_000_000_000,
      wood: 200_000_000,
      straw: 200_000,
      stoneTools: 4_000_000,
      copperTools: 1_000,
      rawHide: 200_000,
      workforce: 4,
      neolithicCulture: 1,
    };
    static resNet = {
      flour: 1,
      stone: -5_000_000,
      stoneTools: -2_500,
      rawHide: -5,
      grain: -2,
    };
    static upgradable = true;
    static upgradeCost = {
      stone: 5_000_000_000,
      wood: 200_000_000,
      straw: 200_000,
      stoneTools: 1_000_000,
      copperTools: 200,
      rawHide: 50_000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = lateNeolithicCopperPhase;
  },

  class ClayOven extends Building {
    static className = 'ClayOven';
    static description = 'An improved version of earth oven - much cleaner and efficient. \n' +
      'Best for baking a dough with meat.';
    static buildCost = {
      stone: 5_000_000_000,
      clay: 100_000_000,
      straw: 20_000_000,
      rawHide: 100_000,
      copperTools: 10_000,
      workforce: 6,
      neolithicCulture: 2,
    };
    static resNet = {
      food: 500_000_000_000,
      wood: -5_000_000,
      rawMeat: -30_000,
      rawHide: -8,
      flour: -2,
      copperTools: -1,
    };
    static upgradable = true;
    static upgradeCost = {
      stone: 2_000_000_000,
      clay: 64_000_000,
      straw: 20_000_000,
      copperTools: 10_000,
    };
    static upgradeCostIncrease = 3;
    static upgradeCostIncreaseFunc = incIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = lateNeolithicCopperPhase;
  },

  class FruitCollector extends Building {
    static className = 'FruitCollector';
    static description = 'Collect fruits, but not for eating.';
    static buildCost = {
      wood: 200_000_000,
      basket: 150_000,
      workforce: 15,
      neolithicCulture: 4,
    };
    static resNet = {
      fruit: 1,
      basket: -32,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 200_000_000,
      basket: 20_000,
      workforce: 1,
    };
    static upgradeCostIncrease = 1.5;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = lateNeolithicCopperPhase;
  },

  class FruitBrewer extends Building {
    static className = 'FruitBrewer';
    static description = 'Why is the sun so loud? I shouldn\'t have drank that. \n' +
      'Hmm, is there anything left?';
    static buildCost = {
      wood: 16 * 10 ** 9,
      stone: 2 * 10 ** 9,
      basket: 200_000,
      copperTools: 5_000,
      rawHide: 100_000,
      workforce: 5,
      neolithicCulture: 6,
    };
    static resNet = {
      fruitLiquor: 1,
      copperTools: -1,
      fruit: -4,
      wood: -2_000_000,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 200_000_000,
      basket: 20_000,
      rawHide: 50_000,
      copperTools: 150,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = lateNeolithicCopperPhase;
  },

];
