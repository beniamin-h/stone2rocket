export const settlementPhase = 'settlement';
export const collectorsPhase = 'collectors';
export const believersPhase = 'believers';
export const stonePhase = 'stone';
export const neolithicArtistsPhase = 'neolithicArtists';
export const lateNeolithicArtistsPhase = 'lateNeolithicArtists';
export const neolithicFirePhase = 'neolithicFire';
export const lateNeolithicStonePhase = 'neolithicStone';
export const lateNeolithicCopperPhase = 'neolithicCopper';
export const earlyChalcolithicPhase = 'earlyChalcolithic';
export const middleChalcolithicPhase = 'middleChalcolithic';
export const lateChalcolithicPhase = 'lateChalcolithic';

export const sortedPhases = [
  settlementPhase,
  collectorsPhase,
  believersPhase,
  stonePhase,
  neolithicArtistsPhase,
  neolithicFirePhase,
  lateNeolithicStonePhase,
  lateNeolithicArtistsPhase,
  lateNeolithicCopperPhase,
  earlyChalcolithicPhase,
  middleChalcolithicPhase,
  lateChalcolithicPhase,
];

export function isPhaseLater(phaseA, phaseB) {
  return sortedPhases.indexOf(phaseA) > sortedPhases.indexOf(phaseB);
}

export function isNextPhase(phaseA, phaseB) {
  return getNextPhase(phaseB) === phaseA;
}

export function getNextPhase(phase) {
  return sortedPhases[sortedPhases.indexOf(phase) + 1];
}
