import {noIncrease} from '@/utils/increaseFunc';

export default class Building {
  static className = 'Undefined';
  static description = '';
  static displayName = '';
  static codeName = '';
  static maxLevelDiff = 500;

  static buildCost = {};
  static buildCostIncrease = 1;
  static buildCostIncreaseFunc = noIncrease;
  static buildGain = {};
  static resNet = {};

  static upgradable = false;
  static upgradeCost = {};
  static upgradeCostIncrease = 1;
  static upgradeCostIncreaseFunc = noIncrease;
  static upgradeGain = {};
  static upgradeGainIncrease = 1;
  static upgradeGainIncreaseFunc = noIncrease;
  static upgradeNetIncrease = 1;
  static upgradeNetIncreaseFunc = noIncrease;

  static affordable = false;
  static constructible = true;
  static destructible = true;
  static developmentPhase = null;

  mapX;
  mapY;
  level = 1;
  displayName;
  codeName;
  upgradable;
  disabledByInsufficientRes = false;
  insufficientRes = '';
  affordLevelUp = false;

  constructor(mapX, mapY, baseLevel = 1) {
    this.mapX = mapX;
    this.mapY = mapY;
    this.level = baseLevel;
    this.displayName = this.constructor.displayName;
    this.codeName = this.constructor.codeName;
    this.upgradable = this.constructor.upgradable;
  }

  static calcExternalMultiplier(externalMultiplierFactor, resAmt) {
    return Math.round(Math.pow(
      externalMultiplierFactor, 1 - 50 / (resAmt + 54)
    ) * 2) / 2;
  }

  static getBuildCost(buildingProvider, current = false, externalMultiplierFactor = 1) {
    const forHowManyBuilt = (buildingProvider.builtCounters[this.className] || 0) + (current ? -1 : 0);
    const counterCostMultiplier = this.buildCostIncreaseFunc(this.buildCostIncrease, forHowManyBuilt);
    const cost = {};
    for (const resName in this.buildCost) {
      const resAmt = this.buildCost[resName];
      const externalMultiplier = this.calcExternalMultiplier(externalMultiplierFactor, resAmt);
      cost[resName] = Math.ceil(resAmt * counterCostMultiplier * externalMultiplier);
    }
    return cost;
  }

  static canAfford(buildingProvider, resStore) {
    return Object.entries(this.getBuildCost(buildingProvider)).every(([resName, resAmt]) => {
      return resAmt <= resStore.store[resName];
    });
  }

  canAffordDemolish(resStore) {
    return Object.entries(this.getTotalGain()).every(([resName, resAmt]) => {
      return resAmt <= resStore.store[resName];
    });
  }

  static getLevelNetMultiplier(level) {
    return this.upgradeNetIncreaseFunc(
      this.upgradeNetIncrease, level - 1);
  }

  static getBuildingExpNetMultiplier(exp) {
    return 1 + exp.getLevel(
      exp.GET_KIND.BUILDING,
      this.className
    ) / 100;
  }

  static getResExpNetMultiplier(exp) {
    const resExpMultipliers = {};
    for (const resName in this.resNet) {
      resExpMultipliers[resName] = 1 + (this.resNet[resName] > 0
        ? exp.getLevel(exp.GET_KIND.RES, resName) / 100
        : 0);
    }
    return resExpMultipliers;
  }

  static getSettlementDistanceNetMultiplier(settlementDistance) {
    return settlementDistance !== null
      ? Math.floor((1.37 - settlementDistance / 10 / 3) * 100) / 100
      : 1;
  }

  static getResNetMultipliers(level, exp, settlementDistance) {
    return {
      levelMultiplier: this.getLevelNetMultiplier(level),
      buildingExpMultiplier: this.getBuildingExpNetMultiplier(exp),
      resExpMultipliers: this.getResExpNetMultiplier(exp),
      settlementDistanceMultiplier:
        this.getSettlementDistanceNetMultiplier(settlementDistance),
    }
  }

  static getResNet(level, exp, rounded = true, settlementDistance = null) {
    const multipliers = this.getResNetMultipliers(level, exp, settlementDistance);
    const additional = this.getCustomAddedResNet(level);

    const net = {};
    for (const resName in this.resNet) {
      let resAmt = this.resNet[resName] * multipliers.levelMultiplier;
      if (this.resNet[resName] > 0) {
        resAmt *= multipliers.resExpMultipliers[resName];
        resAmt *= multipliers.buildingExpMultiplier;
        resAmt *= multipliers.settlementDistanceMultiplier;
      }
      if (rounded) {
        resAmt = Math.floor(resAmt);
      }
      if (additional[resName]) {
        resAmt += additional[resName];
      }
      net[resName] = resAmt;
    }
    return net;
  }

  static getCustomAddedResNet(level) {
    return {};
  }

  static hasAnyNetProduction(level = 1) {
    const additional = this.getCustomAddedResNet(level);
    return Object.entries(this.resNet).some(([resName, resAmt]) => {
      return resAmt + (additional[resName] || 0) > 0;
    });
  }

  getSettlementDistance(blockMap) {
    return blockMap.getSettlementDistance(this.mapX, this.mapY);
  }

  getResNet(exp, blockMap, rounded = true) {
    const distance = this.getSettlementDistance(blockMap);
    return this.constructor.getResNet(
      this.level, exp, rounded, distance
    );
  }

  getResNetDiff(levelDiff = 0, exp, blockMap) {
    const distance = this.getSettlementDistance(blockMap);
    const oldNet = this.constructor.getResNet(
      this.level, exp, true, distance);
    const newNet = this.constructor.getResNet(
      this.level + levelDiff, exp, true, distance);
    const diff = {};
    for (const resName in oldNet) {
      diff[resName] = newNet[resName] - oldNet[resName];
    }
    return diff;
  }

  getUpgradeCost(levelDiff = 1, externalMultiplierFactor = 1) {
    return Object.entries(this.constructor.upgradeCost).reduce((result, [resName, resAmt]) => {
      let resCost = 0;
      const externalMultiplier = this.constructor.calcExternalMultiplier(externalMultiplierFactor, resAmt);
      if (levelDiff > 0) {
        for (let i = 0; i < levelDiff; i++) {
          resCost += Math.ceil(resAmt * externalMultiplier *
            this.constructor.upgradeCostIncreaseFunc(
              this.constructor.upgradeCostIncrease, this.level + i - 1));
        }
      } else {
        for (let i = 0; i > levelDiff; i--) {
          resCost += Math.ceil(resAmt * externalMultiplier *
            this.constructor.upgradeCostIncreaseFunc(
              this.constructor.upgradeCostIncrease, this.level + i - 2));
        }
      }
      if (resCost !== 0) {
        return {...result, [resName]: resCost};
      } else {
        return result;
      }
    }, {});
  }

  getUpgradeGain(levelDiff = 1) {
    return Object.entries(this.constructor.upgradeGain).reduce((result, [resName, resAmt]) => {
      let resGain = 0;
      if (levelDiff > 0) {
        for (let i = 0; i < levelDiff; i++) {
          resGain += this.constructor.upgradeGainIncreaseFunc(
            this.constructor.upgradeGainIncrease, this.level + i - 1);
        }
      } else {
        for (let i = 0; i > levelDiff; i--) {
          resGain += this.constructor.upgradeGainIncreaseFunc(
            this.constructor.upgradeGainIncrease, this.level + i - 2);
        }
      }
      return {...result, [resName]: Math.floor(resAmt * resGain)};
    }, {});
  }

  getTotalGain() {
    const totalUpgradeGain = Object.entries(this.constructor.upgradeGain).reduce((result, [resName, resAmt]) => {
      let resGain = 0;
      for (let i = 1; i < this.level; i++) {
        resGain += Math.floor(resAmt * this.constructor.upgradeGainIncreaseFunc(
          this.constructor.upgradeGainIncrease, i - 1));
      }
      return { ...result, [resName]: resGain };
    }, {});
    return Object.entries(this.constructor.buildGain)
      .reduce((result, [resName, resAmt]) => {
        const resGain = resAmt + (totalUpgradeGain[resName] || 0);
        return { ...result, [resName]: resGain };
      }, totalUpgradeGain);
  }

  getTotalCost(buildingProvider) {
    const totalUpgradeCost = this.getUpgradeCost(-this.level + 1);
    return Object.entries(this.constructor.getBuildCost(buildingProvider, true))
      .reduce((result, [resName, resAmt]) => {
        const resCost = resAmt + (totalUpgradeCost[resName] || 0);
        return { ...result, [resName]: resCost };
      }, totalUpgradeCost);
  }

  build(buildingProvider, resStore, blockMap, forFree = false) {
    if (!forFree) {
      if (!this.constructor.canAfford(buildingProvider, resStore)) {
        return false;
      }
      Object.entries(this.constructor.getBuildCost(buildingProvider)).forEach(([resName, resAmt]) => {
        resStore.store[resName] -= resAmt;
      });
    }

    Object.entries(this.constructor.buildGain).forEach(([resName, resAmt]) => {
      resStore.store[resName] += resAmt;
    });
    return true;
  }

  demolish(buildingProvider, resStore, force = false) {
    const checkAffordable = !force && !this.disabledByInsufficientRes;
    if (checkAffordable && !this.canAffordDemolish(resStore)) {
      return false;
    }
    while (this.downgrade(resStore, force)) { }
    if (!this.constructor.destructible && !force) {
      return false;
    }
    Object.entries(this.constructor.getBuildCost(buildingProvider, true))
      .forEach(([ resName, resAmt ]) => {
        resStore.store[resName] += resAmt;
      });
    if (!this.disabledByInsufficientRes) {
      Object.entries(this.constructor.buildGain)
        .forEach(([ resName, resAmt ]) => {
          resStore.store[resName] -= resAmt;
        });
    }
    return true;
  }

  canBeUpgraded(resStore, levelDiff = 1) {
    if (!this.upgradable) {
      return false;
    }
    if (levelDiff > this.constructor.maxLevelDiff) {
      return false;
    }
    return Object.entries(this.getUpgradeCost(levelDiff)).every(([resName, resAmt]) => {
      return resAmt <= resStore.store[resName];
    });
  }

  upgrade(resStore) {
    if (!this.canBeUpgraded(resStore)) {
      return false;
    }
    Object.entries(this.getUpgradeCost()).forEach(([resName, resAmt]) => {
      resStore.store[resName] -= resAmt;
    });
    if (!this.disabledByInsufficientRes) {
      Object.entries(this.getUpgradeGain()).forEach(([ resName, resAmt ]) => {
        resStore.store[resName] += resAmt;
      });
    }
    this.level++;
    return true;
  }

  canBeDowngraded(resStore, levelDiff = -1) {
    if (levelDiff < -this.constructor.maxLevelDiff) {
      return false;
    }
    if (this.disabledByInsufficientRes) {
      return true;
    }
    return Object.entries(this.getUpgradeGain(levelDiff)).every(([resName, resAmt]) => {
      return resAmt <= resStore.store[resName];
    });
  }

  downgrade(resStore, force = false) {
    const checkAffordable = !force && !this.disabledByInsufficientRes;
    if (this.level <= 1 || (checkAffordable && !this.canBeDowngraded(resStore, -1))) {
      return false;
    }
    Object.entries(this.getUpgradeCost(-1)).forEach(([resName, resAmt]) => {
      resStore.store[resName] += resAmt;
    });
    if (!this.disabledByInsufficientRes) {
      Object.entries(this.getUpgradeGain(-1)).forEach(([resName, resAmt]) => {
        resStore.store[resName] -= resAmt;
      });
    }
    this.level--;
    return true;
  }

  hasSufficientInputRes(buildingResNet, resStore) {
    let hasSufficient = true;
    for (const [resName, resAmt] of Object.entries(buildingResNet)) {
      if (resAmt < 0 && resStore.store[resName] < -resAmt) {
        if (!this.disabledByInsufficientRes) {
          Object.entries(this.getTotalGain())
            .forEach(([gainResName, gainResAmt]) => {
              resStore.store[gainResName] -= gainResAmt;
              if (resStore.store[gainResName] < 0) {
                // TODO: event - insufficient res: gainResName
              }
            });
          this.disabledByInsufficientRes = true;
        }
        hasSufficient = false;
        this.insufficientRes = resName;
        resStore.markResDeficit(resName);
      }
    }
    if (this.disabledByInsufficientRes && hasSufficient) {
      Object.entries(this.getTotalGain())
        .forEach(([gainResName, gainResAmt]) => {
          resStore.store[gainResName] += gainResAmt;
        });
      this.disabledByInsufficientRes = false;
      this.insufficientRes = '';
    }
    return !this.disabledByInsufficientRes;
  }

  serialize() {
    return {
      className: this.constructor.className,
      mapX: this.mapX,
      mapY: this.mapY,
      level: this.level,
      disabledByInsufficientRes: this.disabledByInsufficientRes,
      insufficientRes: this.insufficientRes,
    };
  }
}
