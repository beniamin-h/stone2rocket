import Building from '@/core/objects/buildings/_base';
import { noIncrease, constIncrease, incIncrease, expIncrease } from '@/utils/increaseFunc';
import {
  earlyChalcolithicPhase, lateChalcolithicPhase, middleChalcolithicPhase,
} from '@/core/objects/buildings/developmentPhases';

export default [

  class MudHut extends Building {
    static className = 'MudHut';
    static description = 'A primitive dwelling built with earth - ' +
      'the beginning of a sedentary lifestyle.';
    static buildCost = {
      food: 500 * 10 ** 12,
      straw: 15_000_000,
      clay: 15_000_000,
      animalBone: 1_000_000,
      string: 500_000,
      rawHide: 100_000,
      copperTools: 100,
    };
    static buildCostIncrease = 1.2;
    static buildCostIncreaseFunc = expIncrease;
    static buildGain = {
      workforce: 5,
      chalcolithicCulture: 1,
    };
    static resNet = {
      food: -5 * 10 ** 12,
      wood: -500_000_000,
      peltClothing: -10,
      fruitLiquor: -1,
    };
    static upgradable = true;
    static upgradeCost = {
      food: 2 * 10 ** 15,
      wood: 20 * 10 ** 9,
      stone: 20 * 10 ** 9,
      straw: 25_000_000,
      clay: 15_000_000,
      animalBone: 10_000_000,
      rawHide: 100_000,
      copperTools: 1000,
      pottery: 200,
      waterSupply: 1,
    };
    static upgradeCostIncrease = 1;
    static upgradeCostIncreaseFunc = incIncrease;
    static upgradeGain = {
      chalcolithicCulture: 1,
      workforce: 5,
    };
    static upgradeGainIncrease = 1;
    static upgradeGainIncreaseFunc = noIncrease;
    static upgradeNetIncrease = 5;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = earlyChalcolithicPhase;
  },

  class PotterHut extends Building {
    static className = 'PotterHut';
    static description = 'Make basic pottery from clay.';
    static buildCost = {
      wood: 100 * 10 ** 9,
      stone: 25 * 10 ** 9,
      straw: 10_000_000,
      clay: 20_000_000,
      rawHide: 100_000,
      copperTools: 2000,
      workforce: 10,
      chalcolithicCulture: 2,
    };
    static resNet = {
      wood: -1 * 10 ** 9,
      clay: -50_000,
      pottery: 1,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 100 * 10 ** 9,
      stone: 25 * 10 ** 9,
      straw: 10_000_000,
      clay: 2_000_000,
      rawHide: 100_000,
      copperTools: 1000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = earlyChalcolithicPhase;
  },

  class ShallowWell extends Building {
    static className = 'ShallowWell';
    static description = 'The primitive source of limited fresh water.';
    static buildCost = {
      wood: 50 * 10 ** 9,
      stone: 250 * 10 ** 9,
      clay: 50_000_000,
      string: 20_000_000,
      straw: 4_000_000,
      rawHide: 200_000,
      copperTools: 500,
      pottery: 500,
    };
    static buildCostIncrease = 1.5;
    static buildCostIncreaseFunc = expIncrease;
    static buildGain = {
      waterSupply: 4,
    };
    static resNet = {
      rawHide: -32,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 20 * 10 ** 9,
      stone: 240 * 10 ** 9,
      clay: 50_000_000,
      string: 25_000_000,
      straw: 1_000_000,
      rawHide: 300_000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeGain = {
      waterSupply: 1,
    };
    static upgradeGainIncrease = 1;
    static upgradeGainIncreaseFunc = noIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = earlyChalcolithicPhase;
  },

  class CopperOreSmelter extends Building {
    static className = 'CopperOreSmelter';
    static description = 'Smelt copper ore into more usable metal bars.';
    static buildCost = {
      stone: 1.2 * 10 ** 12,
      wood: 400 * 10 ** 9,
      clay: 100_000_000,
      straw: 80_000_000,
      rawHide: 300_000,
      copperTools: 10_000,
      pottery: 1000,
      workforce: 16,
      chalcolithicCulture: 2,
    };
    static resNet = {
      copper: 8,
      charcoal: -50_000,
      pottery: -1,
      copperOre: -1,
    };
    static upgradable = true;
    static upgradeCost = {
      stone: 10 ** 12,
      straw: 100_000_000,
      rawHide: 500_000,
      copperTools: 20_000,
    };
    static upgradeCostIncrease = 2.5;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = earlyChalcolithicPhase;
  },

  class CopperToolSmelter extends Building {
    static className = 'CopperToolSmelter';
    static description = 'Make copper tools more efficiently from the refined copper.';
    static buildCost = {
      stone: 1.5 * 10 ** 12,
      wood: 1.3 * 10 ** 12,
      clay: 100_000_000,
      straw: 150_000_000,
      rawHide: 500_000,
      copperTools: 20_000,
      workforce: 15,
      chalcolithicCulture: 3,
    };
    static resNet = {
      copperTools: 15,
      wood: -0.5 * 10 ** 9,
      charcoal: -10_000,
      string: -20_000,
      copper: -2,
    };
    static upgradable = true;
    static upgradeCost = {
      stone: 2.5 * 10 ** 12,
      wood: 0.3 * 10 ** 12,
      clay: 50_000_000,
      straw: 150_000_000,
      rawHide: 1_000_000,
      copperTools: 40_000,
    };
    static upgradeCostIncrease = 2.5;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = earlyChalcolithicPhase;
  },

  class ReedGatherer extends Building {
    static className = 'ReedGatherer';
    static description = 'Amass huge numbers of reed using copper tools.';
    static buildCost = {
      wood: 0.5 * 10 ** 12,
      string: 100_000_000,
      copperTools: 75_000,
      workforce: 20,
      chalcolithicCulture: 2,
    };
    static resNet = {
      reed: 20_000,
      copperTools: -1,
      string: -512_000,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 10 ** 12,
      string: 500_000_000,
      copperTools: 10_000,
    };
    static upgradeCostIncrease = 3;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = earlyChalcolithicPhase;
  },

  class BasketWeaver extends Building {
    static className = 'BasketWeaver';
    static description = 'Best quality baskets.';
    static buildCost = {
      wood: 2.5 * 10 ** 12,
      straw: 250_000_000,
      clay: 20_000_000,
      copperTools: 10_000,
      workforce: 20,
      chalcolithicCulture: 2,
    };
    static resNet = {
      basket: 10_000,
      string: -100_000,
      straw: -50_000,
      reed: -20_000,
    };
    static upgradable = true;
    static upgradeCost = {
      straw: 250_000_000,
      clay: 50_000_000,
      copperTools: 2_000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 3;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = earlyChalcolithicPhase;
  },

  class SandPit extends Building {
    static className = 'SandPit';
    static description = 'Let\'s count grains of sand.';
    static buildCost = {
      wood: 10 * 10 ** 12,
      stone: 3 * 10 ** 12,
      straw: 500_000_000,
      string: 50_000_000,
      copperTools: 50_000,
      workforce: 30,
    };
    static buildCostIncrease = 1.2;
    static buildCostIncreaseFunc = expIncrease;
    static resNet = {
      sand: 5,
      copperTools: -2,
      rawHide: -100,
      basket: -1000,
    };
    static getCustomAddedResNet() {
      return { sand: 5 };
    }
    static upgradable = true;
    static upgradeCost = {
      wood: 15 * 10 ** 12,
      stone: 10 * 10 ** 12,
      straw: 1_000_000_000,
      string: 50_000_000,
      copperTools: 20_000,
      workforce: 2,
    };
    static upgradeCostIncrease = 1.5;
    static upgradeCostIncreaseFunc = incIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = earlyChalcolithicPhase;
  },

  class BrickWorks extends Building {
    static className = 'BrickWorks';
    static description = 'Adding straw and sand to mud allows to form your first bricks.';
    static buildCost = {
      wood: 5 * 10 ** 12,
      stone: 10 ** 12,
      straw: 200_000_000,
      workforce: 15,
      chalcolithicCulture: 3,
    };
    static resNet = {
      brick: 1,
      sand: -2,
      clay: -100_000,
      straw: -200_000,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 3 * 10 ** 12,
      straw: 150_000_000,
      workforce: 1,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = earlyChalcolithicPhase;
  },

  class ReedBoatBuilder extends Building {
    static className = 'ReedBoatBuilder';
    static description = 'Since the reeds are filled with air pockets, \n' +
      'with a little tar coating, they work perfectly for small boat building.';
    static buildCost = {
      wood: 12 * 10 ** 12,
      straw: 200_000_000,
      string: 20_000_000,
      copperTools: 25_000,
      brick: 500,
      workforce: 20,
      chalcolithicCulture: 2,
    };
    static resNet = {
      reedBoat: 1,
      wood: -100_000_000,
      reed: -50_000,
      tar: -200,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 20 * 10 ** 12,
      straw: 200_000_000,
      copperTools: 100_000,
      brick: 1000,
    };
    static upgradeCostIncrease = 2.5;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = earlyChalcolithicPhase;
  },

  class ReedBoatFisherman extends Building {
    static className = 'ReedBoatFisherman';
    static description = 'Now we can catch a fish.';
    static buildCost = {
      wood: 2 * 10 ** 12,
      straw: 100_000_000,
      string: 25_000_000,
      reedBoat: 160,
      workforce: 8,
    };
    static buildCostIncrease = 5;
    static buildCostIncreaseFunc = expIncrease;
    static resNet = {
      food: 20 * 10 ** 12,
      string: -100_000,
      stoneSpear: -1,
    };
    static getCustomAddedResNet() {
      return { food: 80 * 10 ** 12 };
    }
    static upgradable = true;
    static upgradeCost = {
      wood: 10 * 10 ** 12,
      straw: 250_000_000,
      string: 300_000_000,
      reedBoat: 1000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeGain = {
      elementalDeity: 1,
    };
    static upgradeGainIncrease = 1;
    static upgradeGainIncreaseFunc = incIncrease;
    static upgradeNetIncrease = 0.5;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = earlyChalcolithicPhase;
  },

  class WildFruitOrchard extends Building {
    static className = 'WildFruitOrchard';
    static description = 'Keep an eye on some wild trees to collect their sweet fruits.';
    static buildCost = {
      wood: 20 * 10 ** 12,
      stoneTools: 7_500_000,
      string: 2_000_000_000,
      workforce: 12,
      chalcolithicCulture: 1,
    };
    static resNet = {
      fruit: 32,
      basket: -10_000,
      stoneTools: -1000,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 12 * 10 ** 12,
      stoneTools: 777_777,
    };
    static upgradeCostIncrease = 2.5;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = earlyChalcolithicPhase;
  },

  class CopperJeweler extends Building {
    static className = 'CopperJeweler';
    static description = 'Give them jewels and they will do anything you ask.';
    static buildCost = {
      wood: 100 * 10 ** 12,
      straw: 3_000_000_000,
      rawHide: 10_000_000,
      copperTools: 500_000,
      brick: 50_000,
      pottery: 10_000,
      chalcolithicCulture: 10,
      workforce: 7,
    };
    static buildGain = {
      chalcolithicCulture: 5,
    };
    static resNet = {
      string: -50_000,
      copper: -200,
      copperTools: -10,
      preciousStone: -1,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 100 * 10 ** 12,
      straw: 1_500_000_000,
      rawHide: 10_000_000,
      copperTools: 100_000,
      brick: 50_000,
      pottery: 5_000,
    };
    static upgradeCostIncrease = 0.5;
    static upgradeCostIncreaseFunc = incIncrease;
    static upgradeGain = {
      chalcolithicCulture: 3,
      humanDeity: 1,
    };
    static upgradeGainIncrease = 1;
    static upgradeGainIncreaseFunc = noIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = constIncrease;
    static developmentPhase = earlyChalcolithicPhase;
  },

  class WildPasture extends Building {
    static className = 'WildPasture';
    static description = 'Your first attempt to the animal domestication.';
    static buildCost = {
      wood: 30 * 10 ** 12,
      straw: 100_000_000,
      workforce: 5,
    };
    static buildCostIncrease = 1.2;
    static buildCostIncreaseFunc = expIncrease;
    static buildGain = {
      animalDeity: 1,
    };
    static resNet = {
      food: 50 * 10 ** 9,
      animalPelt: 10_000_000,
      rawMeat: 500_000,
      animalBone: 100_000,
      milk: 1,
      straw: -500_000,
      string: -200_000,
      pottery: -2,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 30 * 10 ** 12,
      string: 10 * 10 ** 9,
      straw: 100_000_000,
      copperTools: 20_000,
      waterSupply: 1,
    };
    static upgradeCostIncrease = 0.5;
    static upgradeCostIncreaseFunc = constIncrease;
    static upgradeNetIncrease = 3;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = middleChalcolithicPhase;
  },

  class CheeseMaker extends Building {
    static className = 'CheeseMaker';
    static description = 'Mmm, cheesy.';
    static buildCost = {
      wood: 30 * 10 ** 12,
      straw: 1_000_000_000,
      copperTools: 100_000,
      brick: 1200,
      pottery: 1000,
      chalcolithicCulture: 6,
      workforce: 12,
    };
    static resNet = {
      food: 150 * 10 ** 12,
      rawHide: -50,
      milk: -8,
      pottery: -4,
      copperTools: -1,
    };
    static upgradable = true;
    static upgradeCost = {
      straw: 2_000_000_000,
      wood: 30 * 10 ** 12,
      copperTools: 300_000,
      brick: 2000,
      rawHide: 500_000,
      pottery: 2400,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = incIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = constIncrease;
    static developmentPhase = middleChalcolithicPhase;
  },

  class ButcherHut extends Building {
    static className = 'ButcherHut';
    static description = 'Death to poor animals.';
    static buildCost = {
      wood: 45 * 10 ** 12,
      straw: 2_000_000_000,
      rawHide: 2_500_000,
      copperTools: 500_000,
      pottery: 5000,
      brick: 20_000,
      chalcolithicCulture: 10,
      workforce: 18,
    };
    static resNet = {
      food: 150 * 10 ** 12,
      animalBone: 10_000_000,
      animalPelt: 1_000_000,
      rawMeat: -10_000_000,
      pottery: -2,
      copperTools: -5,
      stoneTools: -20_000,
    };
    static upgradable = true;
    static upgradeCost = {
      straw: 1_000_000_000,
      wood: 45 * 10 ** 12,
      rawHide: 5_000_000,
      copperTools: 600_000,
      brick: 50_000,
      pottery: 10_000,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = middleChalcolithicPhase;
  },

  class Vineyard extends Building {
    static className = 'Vineyard';
    static description = 'An unusual orchard full of particular kind of fruits.';
    static buildCost = {
      wood: 200 * 10 ** 12,
      copperTools: 400_000,
      string: 10 * 10 ** 9,
      workforce: 18,
      chalcolithicCulture: 3,
    };
    static resNet = {
      grapes: 1,
      string: -1_000_000,
      basket: -100_000,
      copperTools: -2,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 400 * 10 ** 12,
      copperTools: 500_000,
      string: 20 * 10 ** 9,
      workforce: 2,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = middleChalcolithicPhase;
  },

  class Winery extends Building {
    static className = 'Winery';
    static description = 'There is a good chance this is wine.';
    static buildCost = {
      wood: 500 * 10 ** 12,
      stone: 200 * 10 ** 12,
      straw: 10 * 10 ** 9,
      string: 2 * 10 ** 9,
      rawHide: 5_000_000,
      copperTools: 1_000_000,
      brick: 200_000,
      pottery: 50_000,
      chalcolithicCulture: 10,
      workforce: 12,
      waterSupply: 1,
    };
    static resNet = {
      wine: 1,
      pottery: -50,
      grapes: -10,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 500 * 10 ** 12,
      stone: 200 * 10 ** 12,
      straw: 10 * 10 ** 9,
      string: 2 * 10 ** 9,
      rawHide: 5_000_000,
      copperTools: 1_000_000,
      brick: 200_000,
      pottery: 50_000,
      chalcolithicCulture: 1,
      workforce: 1,
    };
    static upgradeCostIncrease = 4;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeGain = {
      chalcolithicCulture: 1,
    };
    static upgradeGainIncrease = 1;
    static upgradeGainIncreaseFunc = incIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = middleChalcolithicPhase;
  },

  class ElementalGodShrine extends Building {
    static className = 'ElementalGodShrine';
    static description = 'The divine water spirit is keeping us safe.';
    static buildCost = {
      stone: 10 ** 15,
      brick: 1_000_000,
      copperTools: 100_000,
      pottery: 10_000,
      elementalDeity: 1,
      waterSupply: 1,
    };
    static buildCostIncrease = 2;
    static buildCostIncreaseFunc = expIncrease;
    static buildGain = {
      chalcolithicCulture: 5,
    };
    static resNet = {
      elementalGodWorship: 10,
      food: -1_111_111_111_111_111,
    };
    static upgradable = true;
    static upgradeCost = {
      stone: 3 * 10 ** 15,
      brick: 3_000_000,
      pottery: 30_000,
      elementalDeity: 1,
      workforce: 1,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeGain = {
      elementalGodWorship: 100,
    };
    static upgradeGainIncrease = 10;
    static upgradeGainIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 3;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = middleChalcolithicPhase;
  },

  class AnimalGodShrine extends Building {
    static className = 'AnimalGodShrine';
    static description = 'The mighty bear-god will crush our enemies.';
    static buildCost = {
      wood: 2 * 10 ** 15,
      brick: 10_000_000,
      animalBone: 10 * 10 ** 9,
      animalPelt: 10 * 10 ** 9,
      rawHide: 50_000_000,
      stoneSpear: 10_000,
      animalDeity: 1,
    };
    static buildCostIncrease = 2;
    static buildCostIncreaseFunc = expIncrease;
    static buildGain = {
      chalcolithicCulture: 5,
    };
    static resNet = {
      animalGodWorship: 10,
      milk: -1,
      rawMeat: -500_000_000,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 2 * 10 ** 15,
      brick: 10_000_000,
      animalBone: 10 * 10 ** 9,
      animalPelt: 10 * 10 ** 9,
      rawHide: 50_000_000,
      stoneSpear: 10_000,
      workforce: 2,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeGain = {
      animalGodWorship: 100,
    };
    static upgradeGainIncrease = 10;
    static upgradeGainIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 3;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = middleChalcolithicPhase;
  },

  class HumanGodShrine extends Building {
    static className = 'HumanGodShrine';
    static description = 'So gods created men in their own image.';
    static buildCost = {
      brick: 50_000_000,
      pottery: 100_000,
      copperTools: 300_000,
      humanDeity: 1,
    };
    static buildCostIncrease = 2;
    static buildCostIncreaseFunc = expIncrease;
    static buildGain = {
      chalcolithicCulture: 5,
    };
    static resNet = {
      wine: -2,
      humanGodWorship: 10,
    };
    static upgradable = true;
    static upgradeCost = {
      brick: 10_000_000,
      pottery: 10_000,
      copperTools: 100_000,
      humanDeity: 1,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeGain = {
      humanGodWorship: 100,
    };
    static upgradeGainIncrease = 10;
    static upgradeGainIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 3;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = middleChalcolithicPhase;
  },

  class Bakery extends Building {
    static className = 'Bakery';
    static description = 'Nothing makes your belly as full as a loaf of warm bread.';
    static buildCost = {
      stone: 5 * 10 ** 15,
      wood: 2 * 10 ** 15,
      straw: 20 * 10 ** 9,
      brick: 10_000_000,
      pottery: 100_000,
      copperTools: 500_000,
      chalcolithicCulture: 10,
      workforce: 15,
      waterSupply: 1,
    };
    static resNet = {
      bread: 1,
      wood: -5 * 10 ** 9,
      flour: -1,
    };
    static upgradable = true;
    static upgradeCost = {
      stone: 10 * 10 ** 15,
      wood: 10 ** 15,
      straw: 10 * 10 ** 9,
      brick: 5_000_000,
      pottery: 100_000,
      copperTools: 250_000,
      workforce: 1,
    };
    static upgradeCostIncrease = 2.5;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = middleChalcolithicPhase;
  },

  class ZigguratBuildersCamp extends Building {
    static className = 'ZigguratBuildersCamp';
    static description = 'We need more workers to build the Great Ziggurat.';
    static buildCost = {
      wood: 0.5 * 10 ** 15,
      straw: 10 ** 9,
      pottery: 50_000,
      workforce: 20,
    };
    static resNet = {
      zigguratConstruction: 100,
      food: -1 * 10 ** 15,
      string: -300_000,
      basket: -10_000,
      copperTools: -10,
      bread: -1,
      wine: -1,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 0.5 * 10 ** 15,
      straw: 10 ** 9,
      pottery: 50_000,
      workforce: 20,
    };
    static upgradeCostIncrease = 0;
    static upgradeCostIncreaseFunc = incIncrease;
    static upgradeNetIncrease = 1;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = lateChalcolithicPhase;
  },

  class TabletWorkshop extends Building {
    static className = 'TabletWorkshop';
    static description = '߇ߔߙ ߜߞߧ ߖߔߊ';
    static buildCost = {
      wood: 3 * 10 ** 15,
      stone: 2 * 10 ** 15,
      straw: 25 * 10 ** 9,
      brick: 15_000_000,
      pottery: 20_000,
      copperTools: 400_000,
      chalcolithicCulture: 10,
      workforce: 12,
    };
    static resNet = {
      tablet: 1,
      clay: -1_000_000,
    };
    static upgradable = true;
    static upgradeCost = {
      wood: 10 ** 15,
      stone: 10 ** 15,
      straw: 10 * 10 ** 9,
      brick: 5_000_000,
      pottery: 10_000,
      copperTools: 200_000,
      chalcolithicCulture: 1,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeNetIncrease = 2;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = lateChalcolithicPhase;
  },

  class SlaveMarket extends Building {
    static className = 'SlaveMarket';
    static description = 'The source of a cheap workforce.';
    static buildCost = {
      wood: 10 ** 15,
      straw: 0.5 * 10 ** 9,
      brick: 5_000_000,
      copperTools: 500_000,
      tablet: 1000,
      chalcolithicCulture: 15,
      workforce: 5,
    };
    static resNet = {
      preciousStone: -1,
    };
    static upgradable = true;
    static upgradeCost = {
      preciousStone: 100,
      tablet: 100,
    };
    static upgradeCostIncrease = 2;
    static upgradeCostIncreaseFunc = expIncrease;
    static upgradeGain = {
      workforce: 5,
    };
    static upgradeGainIncrease = 1;
    static upgradeGainIncreaseFunc = constIncrease;
    static upgradeNetIncrease = 10;
    static upgradeNetIncreaseFunc = incIncrease;
    static developmentPhase = lateChalcolithicPhase;
  },

  class TheGreatZiggurat extends Building {
    static className = 'TheGreatZiggurat';
    static displayName = 'The Great Ziggurat';
    static description = 'Upgrade to level 10 to win the demo game.\n ' +
      'Upgrading spends ALL stored &raquo;Ziggurat construction&laquo; resource.';
    static maxLevelDiff = 1;
    static destructible = false;
    static buildCost = {
      wood: 100 * 10 ** 15,
      stone: 100 * 10 ** 15,
      straw: 100 * 10 ** 9,
      brick: 100_000_000,
      rawHide: 100_000_000,
      pottery: 100_000,
      copperTools: 100_000,
      copper: 100_000,
      elementalGodWorship: 1_000_000,
      animalGodWorship: 1_000_000,
      humanGodWorship: 1_000_000,
      zigguratConstruction: 10_000,
      tablet: 100,
      chalcolithicCulture: 20,
      workforce: 50,
    };
    static buildCostIncrease = 10;
    static buildCostIncreaseFunc = expIncrease;
    static resNet = {
      zigguratConstruction: -0.001,
    };
    static upgradable = true;

    static getCustomAddedResNet(level) {
      if (level === 10) {
        return {
          zigguratConstruction: 1_000_000,
        };
      }
      return {};
    }

    canBeUpgraded(resStore, levelDiff = 1) {
      if (this.level + levelDiff > 10) {
        return false;
      }
      if (levelDiff > 1) {
        return false;
      }
      return super.canBeUpgraded(resStore, levelDiff);
    }

    upgrade(resStore) {
      const result = super.upgrade(resStore);
      resStore.store['zigguratConstruction'] = 0;
      return result;
    }

    canBeDowngraded() {
      return false;
    }

    hasSufficientInputRes() {
      return true;
    }

    static upgradeCost = {
      zigguratConstruction: 100_000,
      brick: 100_000_000,
      tablet: 1000,
      chalcolithicCulture: 1,
    };
    static upgradeCostIncrease = 1;
    static upgradeCostIncreaseFunc = constIncrease;
    static upgradeNetIncrease = 10;
    static upgradeNetIncreaseFunc = expIncrease;
    static developmentPhase = lateChalcolithicPhase;
  },

];
