import Vue from 'vue';
import BlockMap from '@/core/objects/blockMap';
import ResStore from '@/core/objects/resStore';
import BuildingProvider from '@/core/objects/buildingProvider';
import { buildings } from '@/core/objects/buildings';
import blockKind from '@/core/consts/blockKind';
import Deferred from '@/utils/deferred';
import Exp from '@/core/objects/exp';
import * as GameState from '@/core/objects/gameState';
import GameEnd from '@/core/objects/gameEnd';
import Stats from '@/core/objects/stats';

export const Game = new Vue({
  data: {
    blockMap: null,
    buildingProvider: null,
    resStore: null,
    exp: null,
    gameEnd: null,
    stats: null,
    initialized: new Deferred(),
    paused: false,
    turn: 0,
  },
  methods: {
    initGame() {
      this.blockMap = new BlockMap();
      this.blockMap.generateMap();
      this.buildingProvider = new BuildingProvider();
      this.resStore = new ResStore();
      this.buildingProvider.initAffordLevelUpUpdater(this.resStore);
      this.exp = new Exp(this.resStore, this.buildingProvider);
      this.resStore.setDependencies(
        this.buildingProvider, this.exp, this.blockMap,
      );
      this.stats = new Stats();
      this.stats.initWatchers(this.buildingProvider.eventBus, this);
      this.gameEnd = new GameEnd();
      this.gameEnd.initVictories(this.buildingProvider.eventBus);
      if (!GameState.loadFromLocalStorage(this)) {
        this.buildSettlement();
      }
      GameState.initAutoSave(this);
      this.initListeners();
      this.initTicker();
      this.initUIListeners();
      this.initialized.resolve();
    },
    buildSettlement() {
      const x = Math.round(Math.random()) * 5 + 2;
      const y = Math.round(Math.random()) * 5 + 2;
      const block = this.blockMap.getBlockByXY(x, y);
      block.kinds = [blockKind.land, blockKind.plain];
      this.buildingProvider.construct(
        block, buildings.Settlement, this.resStore, this.exp, this.blockMap,
      );
    },
    initUIListeners() {
      let selectedToConstruct = null;
      this
        .$on('UI/selectedToConstruct', (building) => {
          selectedToConstruct = building;
        })
        .$on('UI/blockSelected', (block, multiple) => {
          if (block.building) {
            this.$emit('UI/selectedToConstruct', null);
            this.$emit('UI/buildingSelected', block.building);
            return;
          }
          if (selectedToConstruct) {
            this.buildingProvider.construct(
              block, selectedToConstruct, this.resStore, this.exp, this.blockMap,
            );
            if (!multiple) {
              this.$emit('UI/selectedToConstruct', null);
            }
            return;
          }
          this.$emit('UI/buildingSelected', null);
        });
    },
    initListeners() {
      this.gameEnd.eventBus.$on('victory', () => {
        this.paused = true;
      });
    },
    initTicker() {
      this.initialized.then(() => {
        setInterval(() => {
          if (this.paused) {
            return;
          }
          this.resStore.tickResStore();
          this.exp.tickExp();
          this.turn++;
          this.$emit('ticked');
        }, 1000);
      });
    },
    serialize() {
      return {
        blockMap: this.blockMap.serialize(),
        buildingProvider: this.buildingProvider.serialize(),
        resStore: this.resStore.serialize(),
        exp: this.exp.serialize(),
        stats: this.stats.serialize(),
        turn: this.turn,
      };
    },
    load(saveState) {
      this.blockMap.load(saveState.blockMap);
      this.buildingProvider.load(
        saveState.buildingProvider, this.blockMap, this.resStore, this.exp,
      );
      this.resStore.load(saveState.resStore);
      this.exp.load(saveState.exp);
      this.stats.load(saveState.stats);
      this.turn = saveState.turn;
      this.resStore.recalcResNet();
      this.$emit('ticked');
      this.$emit('gameLoaded', null);
    },
  },
});
