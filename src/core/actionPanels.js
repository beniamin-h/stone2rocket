export default {
  'construct': Symbol('construct'),
  'buildingInfo': Symbol('buildingInfo'),
  'expDetails': Symbol('expDetails'),
};
