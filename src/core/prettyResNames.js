import res from '@/core/consts/res'
import { prettyCase } from '@/utils/stringUtils'

export default Object.keys(res).reduce((result, resName) => {
  result[resName] = prettyCase(resName).replaceAll(' ', '\xa0');
  return result;
}, {});
