'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  show_demolish_all_button: 0,
  show_save_load_buttons: 0,
  show_export_import_buttons: 1,
  show_exp_panel: 0,
});
